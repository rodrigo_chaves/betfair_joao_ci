<?
class Transferencias_model extends CI_Model{
	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
    }


function set(){
	$caracteres = array("-", "(", ")"," ");
	$para = str_replace($caracteres, "", $_POST['para']); // login
	$de = str_replace($caracteres, "", $_POST['de']);
	
	if($para == $this->session->userdata('login')){
		redirect('financeiro/transferir/err3' , 'refresh');
	}
	$qr_verifica_login = $this->padrao_model->get_by_matriz('login',$para,'user')->num_rows();
	if($qr_verifica_login == 0){
		redirect('financeiro/transferir/err1' , 'refresh');
	}
	// se telefone não existir
	//  NOVO CADASTRO
	#$de = $_POST['de'];
	#$para = $_POST['para'];
	$descricao = $_POST['descricao'];
	$valor = str_replace("R$ ","",$_POST['valor']);
	$valor = str_replace(".","",$valor);
	$valor = str_replace(",",".",$valor);
	
	$time = date("Y-m-d H:m:s");
	
	// data de liberação
	 // $tipo (1 para formato do DB e 2 para formato PT-BR)
	#$timestamp  = '86400' * $dia + mktime( 0, 0, 0, date( 'm' ), date( 'd' ), date( 'Y' ) );
	#$formato    = ( $tipo == 1 ) ? 'Y-m-d': 'd/m/Y';
	$timestamp  = '86400' * 30 + mktime( 0, 0, 0, date( 'm' ), date( 'd' ), date( 'Y' ) );
	$formato    = 'Y-m-d';
	$data_liberacao = date( $formato, $timestamp );
	#echo $data_liberacao;
	
	
	$cod_de = $this->dd_cliente($this->session->userdata('login'))->id;
	$cod_para = $this->dd_cliente($para)->id;
	
	## VALIDA OPERAÇÃO
	$this->db->where(array('id' => $this->session->userdata('id') , 'senha2' => $_POST['senha2']));
	$qr_senha2 = $this->db->get('user');
	if($qr_senha2->num_rows() == 0){
		redirect('financeiro/transferir/err3' , 'refresh');
	}
	#$de_saldo = $this->extrato($this->dd_cliente($this->session)->cod_ref) + $this->dd_cliente($de)->saldo;
	$de_saldo = $this->extrato($this->session->userdata('id'));
	$para_saldo = $this->extrato($this->dd_cliente($para)->id);
	if($de_saldo < $valor){
		redirect('financeiro/transferir/err2' , 'refresh');
	}else{				
		
		## PORCENTAGEM
	#	$valor = 178.00; // valor original
		$taxa = 5; // quem paga a taxa
		$percentual = $taxa / 100.0; // 3%
		$valor_final = $valor - ($percentual * $valor);
		$juros = $valor - $valor_final;
		
		
		// debita
		/*
		$val_debito = $de_saldo - $valor;
		$dd_debito = array('saldo' => $val_debito);
		$this->db->where('email',$de);
		$this->db->update('clientes' , $dd_debito);
		// Credita
		$val_credito = $para_saldo + $valor_final;
		$dd_credito = array('saldo' => $val_credito);
		$this->db->where('email',$para);
		$this->db->update('clientes' , $dd_credito);
		*/
		// inseri na tabela movimentacao
		$dd = array(
					'descricao' => $descricao,
					'tipo' => 'transferencia',
					'id_user' => $cod_para,
					'id_user_mov' => $cod_de,
					'valor' => $valor,
					'valor_real' => $valor_final,
					'juros' => $juros,
					#'data_hora' => $time,
					#'data_liberacao' => $data_liberacao,
					'status' => 1				
					);
					$this->db->insert('movimento' , $dd);
		
		$dd = array(
					'descricao' => $descricao,
					'tipo' => 'taxa',
					'id_user' => 4,
					'id_user_mov' => $cod_de,
					'valor' => $juros,
					'valor_real' => $valor,
					'juros' => $juros,
					#'data_hora' => $time,
					#'data_liberacao' => $data_liberacao,
					'status' => 1				
					);
					$this->db->insert('movimento' , $dd);
		
		######################### ENVIO DE EMAIL ##############################
		/*
		$dd_debito = $this->padrao_model->get_by_matriz('cod_ref',$cod_de,'clientes')->row();
		$dd_credito = $this->padrao_model->get_by_matriz('cod_ref',$cod_para,'clientes')->row();
		$conteudo = utf8_decode("
		<table width='100%' cellspacing='0' cellpadding='0' border='0' style='width:100.0%;'>
		<tbody>
		  <tr>
			<td><div align='center'>
				<table width='600' cellpadding='0' cellspacing='10' border='0' bgcolor='#FFFFFF'>
				  <tbody>
					<tr>
					  <td style='padding:10px; padding-top:45px; background:#FFFFFF; border:1px solid #dddddd;'>
						<table width='600' cellpadding='0' cellspacing='0'>
						  <tbody>
							<tr>
							  <td style='padding-bottom:5px; padding-top:2px; text-align:center;'><a href='http://www.ubfcard.com.br'> <img style='margin:0;' src='http://www.ubfcard.com.br/imagens/logos/logo10.png'></a></td>
							</tr>
							<tr>
							  <td bgcolor='#ffffff' style='padding:40px;'>
								<h2 style='font-family:Arial;font-size:10pt; padding-bottom:15px;'>Olá ".$dd_credito->responsavel.",</h2>
								<p style='margin-bottom:10px;'> <span style='font-size:10pt; font-family:Arial; color:#000;'> Você recebeu um <b>pagamento</b> atráves do UBF Card Cartão de Débito. </span> </p>
								
								<div style='border:1px solid #333; padding:7px; margin-bottom:15px; background-image:url(http://www.ubfcard.com.br/imagens/pattern_108.gif); font-size:13px;'>
								  <div style='background:#F0F0F0; padding:5px; border:1px solid #333;'>
									<div style='padding-bottom:3px;'><b>Data:</b> ".$time."</div>
									<div style='padding-bottom:3px;'><b>Histórico:</b> ".$descricao."</div>
									<div style='padding-bottom:3px;'><b>Débito:</b> ".$dd_debito->responsavel." (".$dd_debito->email.")</div>
									<div style='padding-bottom:3px;'><b>Crédito:</b> (".$dd_credito->email.")</div>
									<div style='padding-bottom:3px;'><b>Valor:</b> R$ ".number_format($valor,2,',','.')."</div>
									$txt_email
									<div style='padding-bottom:3px;'><b>UBF Card - </b></div>
								  </div>
								</div>
								$txt_email2
								
								<p style='margin-bottom:5px;'> <span style='font-size:10pt; font-family:Arial; color:#000;'> Cordialmente, <br /> Equipe UBF Card.<br /><br /><br /> <b>UBF Card. </b>  <br /> BR 101 Sul, Km 70, Sala. 13 - Recife – PE. (Mini Shopping - CEASA) <br /> CNPJ: 09.096.881//0001-60 <br /> Fones: (81) 3252-1956 </span></p>
							  
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
				  </tbody>
				</table>
			  </div></td>
		  </tr>
	
		</tbody>
	  </table>
		");		
			$this->load->library('email');
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ubfcard.com.br';
			$config['smtp_user'] = 'send@ubfcard.com.br';
			$config['smtp_pass'] = '1234';
			$config['smtp_port'] = '25';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
		
			$this->email->initialize($config);
			$this->email->from("contato@ubfcard.com.br");
			#$this->email->to($_POST['para']); // email de recebimento do contato
			$this->email->to($this->session->userdata('email')); // email de recebimento do contato
			$this->email->subject("Recibo UBF Card"); // titulo
			$this->email->message("$conteudo");	
			#$this->email->send();	
			*/
		} // x else
	
	
	} // x fn

// VENDAS
function set_venda(){
	/*
	if($_POST['para'] == $this->session->userdata('login')){
		redirect('cliente/adm/extrato/normal/err3' , 'refresh');
	}
	*/
	
	$caracteres = array("-", "(", ")"," ");
	$para = str_replace($caracteres, "", $_POST['para']);
	$de = str_replace($caracteres, "", $_POST['de']);
	
	$qr_verifica_telefone = $this->padrao_model->get_by_matriz('telefone1',$de,'clientes');
	// se email não existir
	if($qr_verifica_telefone->num_rows() > 0){
		
		$dd_cliente = $qr_verifica_telefone->row();
		
		}else{
			redirect('cliente/adm/vendas/err0' , 'refresh');
		}
	
	#$de = $_POST['de'];
	#$para = $_POST['para'];
	$descricao = $_POST['descricao'];
	$valor = str_replace("R$ ","",$_POST['valor']);
	$valor = str_replace(".","",$valor);
	$valor = str_replace(",",".",$valor);
	
	$time = date("Y-m-d H:m:s");
	
	$timestamp  = '86400' * 30 + mktime( 0, 0, 0, date( 'm' ), date( 'd' ), date( 'Y' ) );
	$formato    = 'Y-m-d';
	$data_liberacao = date( $formato, $timestamp );
		
	$cod_de = $this->dd_cliente($de)->cod_ref;
	$cod_para = $this->dd_cliente($para)->cod_ref;
	
	## VALIDA OPERAÇÃO
	$this->db->where(array('telefone1' => $de , 'senha2' => $_POST['senha2']));
	$qr_senha2 = $this->db->get('clientes');
	if($qr_senha2->num_rows() == 0){
		redirect('cliente/adm/vendas/err2' , 'refresh');
		}
		#echo $this->dd_cliente($para)->cod_ref;
		#return false;
	#$de_saldo = $this->extrato($this->dd_cliente($de)->cod_ref);
	$de_saldo = $this->extrato($this->dd_cliente($de)->cod_ref) + $this->dd_cliente($de)->saldo;
	$para_saldo = $this->extrato($this->dd_cliente($para)->cod_ref);
	if($de_saldo < $valor){
		redirect('cliente/adm/vendas/err1' , 'refresh');
	}else{				
			
			## PORCENTAGEM
		#	$valor = 178.00; // valor original
			$taxa = $this->dd_cliente($para)->taxa;
			$percentual = $taxa / 100.0; // 3%
			$valor_final = $valor - ($percentual * $valor);
			$juros = $valor - $valor_final;
			
			
			// debita
			$val_debito = $de_saldo - $valor;
			$dd_debito = array('saldo' => $val_debito);
			$this->db->where('email',$de);
			$this->db->update('clientes' , $dd_debito);
			// Credita
			$val_credito = $para_saldo + $valor_final;
			$dd_credito = array('saldo' => $val_credito);
			$this->db->where('email',$para);
			$this->db->update('clientes' , $dd_credito);
			
			// inseri na tabela movimentacao
			$dd = array(
						'descricao' => $descricao,
						'cod_conta_credito' => $cod_para,
						'cod_conta_debito' => $cod_de,
						'valor' => $valor_final,
						'valor_real' => $valor,
						'juros' => $juros,
						'data_hora' => $time,
						'data_liberacao' => $data_liberacao,
						'status' => 1				
						);
						$this->db->insert('movimento' , $dd);
		
		
			
			######################### ENVIO DE EMAIL ##############################
			$txt_email = "";
			$txt_email2 = "";
			$dd_debito = $this->padrao_model->get_by_matriz('cod_ref',$cod_de,'clientes')->row();
			$dd_credito = $this->padrao_model->get_by_matriz('cod_ref',$cod_para,'clientes')->row();
			$conteudo = utf8_decode("
			<table width='100%' cellspacing='0' cellpadding='0' border='0' style='width:100.0%;'>
			<tbody>
			  <tr>
				<td><div align='center'>
					<table width='600' cellpadding='0' cellspacing='10' border='0' bgcolor='#FFFFFF'>
					  <tbody>
						<tr>
						  <td style='padding:10px; padding-top:45px; background:#FFFFFF; border:1px solid #dddddd;'>
							<table width='600' cellpadding='0' cellspacing='0'>
							  <tbody>
								<tr>
								  <td style='padding-bottom:5px; padding-top:2px; text-align:center;'><a href='http://www.ubfcard.com.br'> <img style='margin:0;' src='http://www.ubfcard.com.br/imagens/logos/logo10.png'></a></td>
								</tr>
								<tr>
								  <td bgcolor='#ffffff' style='padding:40px;'>
									<h2 style='font-family:Arial;font-size:10pt; padding-bottom:15px;'>Olá ".$_POST['para'].",</h2>
									<p style='margin-bottom:10px;'> <span style='font-size:10pt; font-family:Arial; color:#000;'> Você recebeu um <b>pagamento</b> atráves do UBF Card Cartão de Débito. </span> </p>
									
									<div style='border:1px solid #333; padding:7px; margin-bottom:15px; background-image:url(http://www.ubfcard.com.br/imagens/pattern_108.gif); font-size:13px;'>
									  <div style='background:#F0F0F0; padding:5px; border:1px solid #333;'>
										<div style='padding-bottom:3px;'><b>Data:</b> ".$time."</div>
										<div style='padding-bottom:3px;'><b>Histórico:</b> ".$descricao."</div>
										<div style='padding-bottom:3px;'><b>Débito:</b> ".$dd_debito->responsavel." (".$dd_debito->email.")</div>
										<div style='padding-bottom:3px;'><b>Crédito:</b> (".$dd_credito->email.")</div>
										<div style='padding-bottom:3px;'><b>Valor:</b> R$ ".number_format($valor,2,',','.')."</div>
										$txt_email
										<div style='padding-bottom:3px;'><b>UBF Card - </b></div>
									  </div>
									</div>
									$txt_email2
									
									<p style='margin-bottom:5px;'> <span style='font-size:10pt; font-family:Arial; color:#000;'> Cordialmente, <br /> Equipe UBF Card.<br /><br /><br /> <b>UBF Card. </b>  <br /> BR 101 Sul, Km 70, Sala. 13 - Recife – PE. (Mini Shopping - CEASA) <br /> CNPJ: 09.096.881//0001-60 <br /> Fones: (81) 3252-1956 </span></p>
								  
								  </td>
								</tr>
							  </tbody>
							</table>
						  </td>
						</tr>
					  </tbody>
					</table>
				  </div></td>
			  </tr>
		
			</tbody>
		  </table>
			");		
			$this->load->library('email');
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ubfcard.com.br';
			$config['smtp_user'] = 'send@ubfcard.com.br';
			$config['smtp_pass'] = '1234';
			$config['smtp_port'] = '25';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
		
			$this->email->initialize($config);
			$this->email->from("contato@ubfcard.com.br");
			$this->email->to($dd_credito->email); // email de recebimento do contato
			$this->email->subject("Recibo UBF Card"); // titulo
			$this->email->message("$conteudo");	
			#$this->email->send();	
		} // x else
	
	
	} // x fn

function get_historico($id_user=""){
	if($id_user == ""){
		$id_user = $this->session->userdata('id');
	}
	#if($tipo == 'liberado'){
	#	$qr = $this->db->query("SELECT * FROM movimento WHERE (cod_conta_credito = '$conta' OR cod_conta_debito = '$conta') AND status = '1' AND data_liberacao < NOW() order by id asc");	
	#}else{
		$qr = $this->db->query("SELECT * FROM movimento WHERE (id_user = '$id_user' OR id_user_mov = '$id_user') AND status = '1' order by id desc");
	#}
	return $qr;
}

function get_historico_transfer($id_user=""){
	if($id_user == ""){
		$id_user = $this->session->userdata('id');
	}
	#if($tipo == 'liberado'){
	#	$qr = $this->db->query("SELECT * FROM movimento WHERE (cod_conta_credito = '$conta' OR cod_conta_debito = '$conta') AND status = '1' AND data_liberacao < NOW() order by id asc");	
	#}else{
		$qr = $this->db->query("SELECT * FROM movimento WHERE (id_user = '$id_user' OR id_user_mov = '$id_user') AND (tipo = 'transferencia') AND status = '1' order by id desc");
	#}
	return $qr;
}

function set_saque(){
	$de = "saque@ubfcard.com.br";
	$para = $this->padrao_model->get_by_id($this->session->userdata('id'),'clientes')->row()->telefone1;
	$descricao = "Saque";
	$valor = str_replace("R$ ","",$_POST['valor']);
	$valor = str_replace(".","",$valor);
	$valor = str_replace(",",".",$valor);
	$time = date("Y-m-d H:m:s");
	#$cod_de = $this->dd_cliente($de)->cod_ref;
	$cod_de = $this->padrao_model->get_by_matriz('email',$de,'clientes')->row()->cod_ref;
	$cod_para = $this->dd_cliente($para)->cod_ref;
	
	## VALIDA OPERAÇÃO
#	$de_saldo = $this->dd_cliente($de)->saldo;
#	$para_saldo = $this->dd_cliente($para)->saldo;
	
	#$de_saldo = $this->extrato($this->dd_cliente($de)->cod_ref);
	$de_saldo = $this->extrato($cod_de);
	$para_saldo = $this->extrato($this->dd_cliente($para)->cod_ref,'liberado');
	
	if($valor > $para_saldo){
		redirect('cliente/adm/saques/err1' , 'refresh');
	}else{				
		
		## PORCENTAGEM
#	 	$valor = 178.00; // valor original
#		$taxa = $this->padrao_model->get_qr('taxas')->row()->debitacao;
#		$percentual = $taxa / 100.0; // 3%
		$valor_final = $valor;
		$juros = $valor - $valor_final;
		
		
		// debita
		$val_debito = $para_saldo - $valor_final;
		$dd_debito = array('saldo' => $val_debito);
		$this->db->where('email',$para);
		$this->db->update('clientes' , $dd_debito);
		// Credita
		#$val_credito = $para_saldo + $valor_final;
		#$dd_credito = array('saldo' => $val_credito);
		#$this->db->where('email',$para);
		#$this->db->update('clientes' , $dd_credito);
		
		// inseri na tabela movimentacao
		$dd = array(
					'descricao' => $descricao,
					'cod_conta_credito' => $cod_de,
					'cod_conta_debito' => $cod_para,
					'valor' => $valor_final,
					'valor_real' => $valor,
					'juros' => $juros,
					'data_hora' => $time,
					'status' => 0				
					);
					$this->db->insert('movimento' , $dd);
		
			
		
	} // x else
	
	
	} // x fn

function get_historico_by_data($de='',$ate=''){
	$conta = $this->session->userdata('conta');
	if($_POST){
		$de = $_POST['de'];
		$ate = $_POST['ate'];
	}
	
	$de = $this->padrao_model->converte_data($de)." 00:00:00";
	$ate = $this->padrao_model->converte_data($ate)." 23:59:59";
	
#	$qr = $this->db->query("SELECT * FROM movimento WHERE (cod_conta_credito = '$conta' OR cod_conta_debito = '$conta') AND data_hora BETWEEN '$de' AND '$ate' AND status = '1' order by id asc");
	$this->db->where("data_hora BETWEEN '$de' AND '$ate'");
	$this->db->where("(cod_conta_credito = '$conta' OR cod_conta_debito = '$conta')");
	$this->db->where("status = '1'");
	$qr = $this->db->get('movimento');
	return $qr;
	}

function dd_cliente($login){
	$this->db->where(array('login' => $login));
	$qr = $this->db->get('user');
	return $qr->row();
	}

function dd_cliente_by_conta($conta){
	$this->db->where(array('cod_ref' => $conta));
	$qr = $this->db->get('clientes');
	return $qr->row();
	}

function recibo($id_recibo){
	$this->db->where('id',$id_recibo);
	$this->db->where("id = '$id_recibo' AND (cod_conta_debito = '".$this->session->userdata('conta')."' OR cod_conta_credito = '".$this->session->userdata('conta')."')");
	$qr = $this->db->get('movimento');
	return $qr;
	}

// pegar extrato
function extrato($id_user,$tipo=''){
	$registros = $this->get_historico($id_user);
	
	
	if($tipo == 'transfer'){
		$registros = $this->get_historico_transfer($id_user);
	}
	
	$saldo = 0;
	if($registros->num_rows() > 0){
		
			foreach($registros->result() as $registro){ 
					## CREDITO
					if($registro->id_user == $id_user){
						$dd_de_para = $this->padrao_model->get_by_id($registro->id_user,'user')->row();
						
						$debito = "";
						$saldo += $registro->valor;
						
					}
					## DEBITO
					if($registro->id_user_mov == $id_user){
						
						$dd_de_para = $this->padrao_model->get_by_id($registro->id_user_mov,'user')->row();
						#$debito = "R$ ".number_format($registro->valor_real,2,",",".");
						$credito = "";
						// SALDO
						$saldo -= $registro->valor;
					}
			}
		}
	return $saldo;
	}


// FRONT-END
// VENDAS DE PRODUTOS
function set_venda_produto($id_produto){

	$qr_verifica_email = $this->padrao_model->get_by_matriz('email',$this->session->userdata('email'),'clientes')->num_rows();
	// se email não existir
	if($qr_verifica_email > 0){
		
		$dd_cliente = $this->padrao_model->get_by_matriz('email',$this->session->userdata('email'),'clientes')->row();
		
		}else{
			redirect('cliente/adm/vendas/err0' , 'refresh');
		}
	
	// dados do produto
	$dd_produto = $this->padrao_model->get_by_id($id_produto,'produtos')->row();
	
	// dados do vendedor
	$dd_vendedor = $this->padrao_model->get_by_id($dd_produto->id_user,'clientes')->row();
	
	$de = $dd_cliente->email;
	$para = $dd_vendedor->email;
	$descricao = $dd_produto->descricao;
	#$valor = str_replace("R$ ","",$_POST['valor']);
	$valor = str_replace(".","",$dd_produto->valor);
	$valor = str_replace(",",".",$dd_produto->valor);
	$time = date("Y-m-d H:m:s");
	$cod_de = $this->dd_cliente($de)->cod_ref;
	$cod_para = $this->dd_cliente($para)->cod_ref;
	
	## VALIDA OPERAÇÃO
	$this->db->where(array('id' => $dd_cliente->id , 'senha2' => $_POST['senha2']));
	$qr_senha2 = $this->db->get('clientes');
	if($qr_senha2->num_rows() == 0){
		redirect('home/produto/'.url_title($dd_produto->nome).'/'.$dd_produto->id.'/err2' , 'refresh');
		}
	$de_saldo = $this->extrato($this->dd_cliente($de)->cod_ref);
	$para_saldo = $this->extrato($this->dd_cliente($para)->cod_ref);
	if($de_saldo < $valor){
		redirect('home/produto/'.url_title($dd_produto->nome).'/'.$dd_produto->id.'/err1' , 'refresh');
	}else{				
			
			## PORCENTAGEM
		#	$valor = 178.00; // valor original
			$taxa = $this->dd_cliente($para)->taxa;
			$percentual = $taxa / 100.0; // 3%
			$valor_final = $valor - ($percentual * $valor);
			$juros = $valor - $valor_final;
			
			
			// debita
			$val_debito = $de_saldo - $valor;
			$dd_debito = array('saldo' => $val_debito);
			$this->db->where('email',$de);
			$this->db->update('clientes' , $dd_debito);
			// Credita
			$val_credito = $para_saldo + $valor_final;
			$dd_credito = array('saldo' => $val_credito);
			$this->db->where('email',$para);
			$this->db->update('clientes' , $dd_credito);
			
			// inseri na tabela movimentacao
			$dd = array(
						'descricao' => $dd_produto->nome,
						'cod_conta_credito' => $cod_para,
						'cod_conta_debito' => $cod_de,
						'valor' => $valor_final,
						'valor_real' => $valor,
						'juros' => $juros,
						'data_hora' => $time,
						'status' => 1				
						);
						$this->db->insert('movimento' , $dd);
		
			// REMOVI ESTOQUE
			$this->db->query("UPDATE produtos SET `qtd_estoque`= qtd_estoque-1 WHERE `id`=".$dd_produto->id."");
			
			######################### ENVIO DE EMAIL ##############################
			$txt_email = "";
			$txt_email2 = "";
			$dd_debito = $this->padrao_model->get_by_matriz('cod_ref',$cod_de,'clientes')->row();
			$dd_credito = $this->padrao_model->get_by_matriz('cod_ref',$cod_para,'clientes')->row();
			$conteudo = utf8_decode("
			<table width='100%' cellspacing='0' cellpadding='0' border='0' style='width:100.0%;'>
			<tbody>
			  <tr>
				<td><div align='center'>
					<table width='600' cellpadding='0' cellspacing='10' border='0' bgcolor='#FFFFFF'>
					  <tbody>
						<tr>
						  <td style='padding:10px; padding-top:45px; background:#FFFFFF; border:1px solid #dddddd;'>
							<table width='600' cellpadding='0' cellspacing='0'>
							  <tbody>
								<tr>
								  <td style='padding-bottom:5px; padding-top:2px; text-align:center;'><a href='http://www.ubfcard.com.br'> <img style='margin:0;' src='http://www.ubfcard.com.br/imagens/logos/logo10.png'></a></td>
								</tr>
								<tr>
								  <td bgcolor='#ffffff' style='padding:40px;'>
									<h2 style='font-family:Arial;font-size:10pt; padding-bottom:15px;'>Olá ".$dd_cliente->responsavel.",</h2>
									<p style='margin-bottom:10px;'> <span style='font-size:10pt; font-family:Arial; color:#000;'> Você recebeu um <b>pagamento</b> atráves do UBF Card Cartão de Débito. </span> </p>
									
									<div style='border:1px solid #333; padding:7px; margin-bottom:15px; background-image:url(http://www.ubfcard.com.br/imagens/pattern_108.gif); font-size:13px;'>
									  <div style='background:#F0F0F0; padding:5px; border:1px solid #333;'>
										<div style='padding-bottom:3px;'><b>Data:</b> ".$time."</div>
										<div style='padding-bottom:3px;'><b>Histórico:</b> ".$descricao."</div>
										<div style='padding-bottom:3px;'><b>Débito:</b> ".$dd_debito->responsavel." (".$dd_debito->email.")</div>
										<div style='padding-bottom:3px;'><b>Crédito:</b> (".$dd_credito->email.")</div>
										<div style='padding-bottom:3px;'><b>Valor:</b> R$ ".number_format($valor,2,',','.')."</div>
										$txt_email
										<div style='padding-bottom:3px;'><b>UBF Card - </b></div>
									  </div>
									</div>
									$txt_email2
									
									<p style='margin-bottom:5px;'> <span style='font-size:10pt; font-family:Arial; color:#000;'> Cordialmente, <br /> Equipe UBF Card.<br /><br /><br /> <b>UBF Card. </b>  <br /> BR 101 Sul, Km 70, Sala. 13 - Recife – PE. (Mini Shopping - CEASA) <br /> CNPJ: 09.096.881//0001-60 <br /> Fones: (81) 3252-1956 </span></p>
								  
								  </td>
								</tr>
							  </tbody>
							</table>
						  </td>
						</tr>
					  </tbody>
					</table>
				  </div></td>
			  </tr>
		
			</tbody>
		  </table>
			");		
			$this->load->library('email');
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ubfcard.com.br';
			$config['smtp_user'] = 'send@ubfcard.com.br';
			$config['smtp_pass'] = '1234';
			$config['smtp_port'] = '25';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
		
			$this->email->initialize($config);
			$this->email->from("contato@ubfcard.com.br");
			$this->email->to($dd_vendedor->email); // email de recebimento do contato
			$this->email->subject("Recibo UBF Card"); // titulo
			$this->email->message("$conteudo");	
			$this->email->send();	
		} // x else
	
	
	} // x fn


############### BOLETO

function gerar_boleto($id_ass = "", $mes = "", $ano = "") {
        if ($id_ass == "" or $id_ass == "null") {
            $id_ass = $this->session->userdata('id');
        }
        //dd associado
        $dd_associado = $this->padrao_model->get_by_matriz('id', $id_ass, 'clientes')->row();
        // verificar data para definir o valor
        if (!isset($_POST['valor'])) {
            #$valor_fn = 5.00;
			$valor_fn = abs($this->transferencias_model->extrato($this->session->userdata('conta'),'fatura'));
        } else {
            #$valor_fn = number_format($_POST['valor'],2,',','.');
            $valor_fn = str_replace(",", ".", $_POST['valor']);
        }

        // seta a data 
        #$dia = "30";	
#	if(!isset($_POST['mes'])){
        $data_fn = date("Y-m-d");
        // ano bissesto
        if ($mes == "2" || $mes == "02") {
            $dia = "28";
        }
        // vencimento 
        $dia = "15";
        $mes = date('m');
        $ano = date('Y');
        // caso no seja no mes do pagemento
        if (date("d") > 10) {
            $mes = $mes + 1;
        }
        // virada de ano
        if ($mes == 13) {
            $mes = 01;
            $ano = $ano + 1;
        }

        $dat_venc = $dia . "/" . $mes . '/' . $ano;
        #echo $dat_venc."()";
        // seta descricao do pagamento
        if (isset($_POST['descricao'])) {
            $descricao = $_POST['descricao'];
        } else {
            $descricao = "";
        }
        $dd = array(
            'id_ass' => $id_ass,
            'vencimento_fn' => $this->padrao_model->converte_data($dat_venc),
            'status_fn' => 'aguardando',
            'valor_fn' => $valor_fn,
            'data_fn' => $data_fn,
            'descricao' => $descricao
        );

        $this->db->insert('financeiro', $dd);
        #$this->db->order_by('id_fn','desc');
#	$this->db->limit(1);
#	$this->db->where('id_ass',$id_ass);
#	$qr_ass = $this->db->get('financeiro')->row();
#	$last = $qr_ass->id_fn;
        $last = $this->db->insert_id();
        // nosso numero
        #$nn = $id_ass.$mes.$ano."0".$last;
        // get sufixo data para id_ps

        $nn = $last;
        $dd_nn = array('id_ps' => $nn);
        $this->db->where('id_fn', $last);
        $this->db->update('financeiro', $dd_nn);
        return $last;
    }



} // fecha class
?>