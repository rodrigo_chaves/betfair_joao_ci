<?php
class Informativos_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}
	/*
	function getNoticias($limit=99){

		$qr = $this->db->query("SELECT * FROM noticias LIMIT $limit");		
		return $qr;	
	}
	
	function getNoticiasArea($limit=99, $area='pub_home'){

		$qr = $this->db->query("SELECT * FROM noticias WHERE $area=1 LIMIT $limit");		
		return $qr;	
		
	}	
	
	function getPremios() {		
		
		$qr = $this->db->query("SELECT * FROM noticias WHERE categoria = '$categoria'");		
		return $qr;			
	
	}
	*/
	
	function cadastrar(){
		$dd = array('titulo' => $this->input->post('titulo'),
					'chamada' => $this->input->post('chamada'),
					#'tipo' => $this->input->post('tipo'),
					'conteudo' => $this->input->post('conteudo'),
					'status' => $this->input->post('status'),
					'id_user' => $this->session->userdata('id')				
					);
		

		if ($this->db->insert('informativos', $dd)) {
			#return true;
			$id_informativo = $this->db->insert_id();
			
			// multiplupload
			################## ANEXOS		
			if ($anexos = $this->input->post('anexos')) {		
				foreach ($anexos as $anexo) :
					$dd_up = array(
						'id_informativo' => $id_informativo,
						'anexo' => $anexo								
					);
					#print_r($dd_up).'<br><br>';
					$this->db->insert('informativos_anexos', $dd_up);					
				endforeach;		
			}
			
			redirect('adm/informativos/enviar/'.$id_informativo);
		} else {
		#	return false;
		}
	}
	
	
	function editar(){
		
		$dd = array('titulo' => $this->input->post('titulo'),
					'chamada' => $this->input->post('chamada'),
					#'tipo' => $this->input->post('tipo'),
					'conteudo' => $this->input->post('conteudo'),
					'status' => $this->input->post('status')
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img'] = $_POST['imagem'];
		}
		
		$this->db->where('id', $_POST['id']);
		if ($this->db->update('informativos', $dd)) {
			#return true;
			
			if ($anexos = $this->input->post('anexos')) {		
				foreach ($anexos as $anexo) :
					$dd_up = array(
						'id_informativo' => $_POST['id'],
						'anexo' => $anexo								
					);
					#print_r($dd_up).'<br><br>';
					$this->db->insert('informativos_anexos', $dd_up);					
				endforeach;		
			}
			
			
		} else {
			#return false;
		}
		redirect('adm/informativos');
	}
	
	function set_envio(){
		$id_informativo = $_POST['id_informativo'];
		$setores = $_POST['setores'];
		$unidades = $_POST['unidades'];
		
		foreach($unidades as $key => $unidade){
			#echo $unidade.'<br />';
			
			foreach($setores as $key => $setor){
				#echo $setor.'<br />';
				
				$dd_info = array(
					'id_informativo' => $id_informativo,
					'id_unidade' => $unidade,
					'id_setor' => $setor,
				);
				$this->db->insert('informativos_usuarios' , $dd_info);
				
				// envio e-mail
				$this->db->where(array('id_unidade' => $unidade , 'id_setor' => $setor));
				$qr_sends = $this->db->get('usuarios');
				if($qr_sends->num_rows() > 0){
					foreach($qr_sends->result() as $send){
						$nome = $send->nome;
						$email = $send->email;
						$dd_info = $this->padrao_model->get_by_id($id_informativo,'informativos')->row();
						
						
						
						################# ENVIO DO EMAIL		
						$conteudo = utf8_decode("
						<div align='center'>
							<p align='center' style='text-align:center'>
							<h3>".$dd_info->titulo."</h3>
							<a href='".base_url()."admin' target='_blank'>
								<img src='".base_url()."images/news.jpg' alt='Informativo Mararilha Motos'>
							</a>
							</p>
							
						</div>						
						");
						#$conteudo = $this->base_email($texto,$titulo);
						
						$this->load->library('email');
						$config['protocol'] = 'mail';
						$config['mailpath'] = '/usr/sbin/sendmail';
						
						$config['charset'] = 'iso-8859-1';
						//$config['wordwrap'] = TRUE;
						$config['mailtype'] = 'html';
				
						$this->email->initialize($config);
						$this->email->from("maravilhamotos@maravilhamotos.com.br", "Maravilha Motos");
						$this->email->to($send->email); // email de recebimento do contato
						#$this->email->to("igor@nuvemlab.com.br"); // email de recebimento do contato
						#$this->email->bcc("");			
						$this->email->subject(utf8_decode("Informativo - Maravilha Motos")); // titulo
						$this->email->message($conteudo);
						
						$this->email->send();
			
						
						
						
					}
				}
				
				
			}
			
		}
		redirect('adm/informativos/enviar/'.$id_informativo);
		
		
		
		
		
		
	}
	
	function remover($id) {
		
		$this->db->where('id', $id);
		$this->db->delete('informativos');	
		
	}

}
?>