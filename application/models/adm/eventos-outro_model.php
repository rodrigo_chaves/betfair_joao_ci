<?php
class Eventos_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}

	function getProximoEvento() {
		
		$qr = $this->db->query('SELECT * FROM eventos WHERE data > now() ORDER BY id desc');
		return $qr;
	
	}

	function getProximosEventos($limit) {
		
		$qr = $this->db->query('SELECT * FROM eventos WHERE data > now() ORDER BY data LIMIT '.$limit);
		return $qr;
	
	}
	
	function getEvento($id) {
		
		$qr = $this->db->query('SELECT * FROM eventos WHERE id = '.$id);
		return $qr;
	
	}
	
	function getImagensEvento($id) {
		
		$qr = $this->db->query('SELECT * FROM eventos_imagens WHERE id_evento = '.$id);
		return $qr;
	
	}
	
	function getMeses() {
	
		$qr = $this->db->query("SELECT id, data, MONTH(data) as mes FROM eventos GROUP BY MONTH(data)");	
		return $qr;
		
	}
	
	function getEventosMes($mes) {
		
		$qr = $this->db->query("SELECT * FROM eventos WHERE MONTH(data) = ".$mes);
		return $qr;
			
	}
	
	function cadastrar() {
		
		$_POST['data'] = $this->converte_datahora($_POST['data']);

		$dd = array('titulo' => $_POST['titulo'],
					'data' => $_POST['data'],
					'local' => $_POST['local'],
					'descricao' => $_POST['descricao'],
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img'] = $_POST['imagem'];
		}
		
		if ($this->db->insert('eventos', $dd)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	function editar(){
		
		$_POST['data'] = $this->converte_datahora($_POST['data']);
		
		$dd = array('titulo' => $_POST['titulo'],
					'data' => $_POST['data'],
					'local' => $_POST['local'],
					'descricao' => $_POST['descricao'],
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img'] = $_POST['imagem'];
		}
		
		$this->db->where('id', $_POST['id']);
		if ($this->db->update('eventos', $dd)) {
			return true;
		} else {
			return false;
		}
	}
	
	function remover($id) {
		
		$this->db->where('id', $id);
		$this->db->delete('eventos');	
		
	}

########### FUNÇÃO PARA PLUGIN DATETIME PICKER #######################
	function converte_datahora($datahora) {
		
		if (strstr($datahora, "/")) {//verifica se tem a barra /
		  
		  $d_h = explode(" ", $datahora); //separa data hora				  
		  $data = $d_h[0];
		  $hora = $d_h[1];
		  $d = explode("/", $data);//tira a barra
		  
		  $invert_data = $d[2].'-'.$d[1].'-'.$d[0].' '.$hora; //separa as datas $d[2] = ano $d[1] = mes etc...
		  
		  return $invert_data;
		
		} elseif(strstr($datahora, "-")) {
		  $d = explode ("-", $data);
		  $invert_data = "$d[2]/$d[1]/$d[0]";
		  return $invert_data;
		} else {
			return "Data invalida"; 
		}
		
	}//
	
}
?>