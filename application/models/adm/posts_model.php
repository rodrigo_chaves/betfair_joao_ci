<?php
class Posts_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}

	function getNoticias($limit=99){

		$qr = $this->db->query("SELECT * FROM noticias LIMIT $limit");		
		return $qr;	
	}
	
	function getNoticiasArea($limit=99, $area='pub_home'){

		$qr = $this->db->query("SELECT * FROM noticias WHERE $area=1 LIMIT $limit");		
		return $qr;	
		
	}	
	
	function getPremios() {		
		
		$qr = $this->db->query("SELECT * FROM noticias WHERE categoria = '$categoria'");		
		return $qr;			
	
	}
	
	function cadastrar(){
		$dd = array('titulo' => $this->input->post('titulo'),
					'chamada' => $this->input->post('chamada'),
					'tipo' => $this->input->post('tipo'),
					'conteudo' => $this->input->post('conteudo'),
					'status' => $this->input->post('status'),				
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img'] = $_POST['imagem'];
		}
		
		if ($this->db->insert('posts', $dd)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	function editar(){
		
		$dd = array('titulo' => $this->input->post('titulo'),
					'chamada' => $this->input->post('chamada'),
					'tipo' => $this->input->post('tipo'),
					'conteudo' => $this->input->post('conteudo'),
					'status' => $this->input->post('status')
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img'] = $_POST['imagem'];
		}
		
		$this->db->where('id', $_POST['id']);
		if ($this->db->update('posts', $dd)) {
			return true;
		} else {
			return false;
		}
	}
	
	function remover($id) {
		
		$this->db->where('id', $id);
		$this->db->delete('posts');	
		
	}

	// VALIDA A NAVEGAÇÃO

########### FUNÇÃO PARA DATAS #######################
	// converter para data dd/mm/aaaa
	function converte_data($data){
		
		if (strstr($data, "/")) {//verifica se tem a barra /
		
		  $d = explode ("/", $data);//tira a barra
		  $invert_data = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
		  return $invert_data;
		
		} elseif(strstr($data, "-")) {
		
		  $d = explode ("-", $data);
		  $invert_data = "$d[2]/$d[1]/$d[0]";
		  return $invert_data;
		
		} else {
		
		  return "Data invalida";
		
		}
	
	}
	
}
?>