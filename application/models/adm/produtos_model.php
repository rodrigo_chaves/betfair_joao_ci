<?php
class Produtos_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}
	
	function getProdutosCategoria($categoria){

		$qr = $this->db->query("SELECT * FROM produtos WHERE categoria = ".$categoria);		
		return $qr;	
	}
		
	function getCategorias(){

		$qr = $this->db->query("SELECT n.categoria, (	SELECT COUNT( * ) 
													FROM noticias
													WHERE categoria = n.categoria
													) as qtd
													FROM noticias n
													GROUP BY n.categoria");
		
		return $qr;	
	}
	
	
	function cadastrar(){
		//print_r($this->input->post('cores'));
		
		$dd = array('modelo' => $this->input->post('modelo'),
					'id_categoria' => $this->input->post('categoria'),
					'caracteristicas' => $this->input->post('caracteristicas'),
					'especificacoes' => $this->input->post('especificacoes')
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img_portfolio'] = $_POST['imagem'];
		}
		
		if ($this->db->insert('produtos', $dd)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	function editar(){
		
		$dd = array('modelo' => $this->input->post('modelo'),
					'id_categoria' => $this->input->post('categoria'),
					'caracteristicas' => $this->input->post('caracteristicas'),
					'especificacoes' => $this->input->post('especificacoes')
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img_portfolio'] = $_POST['imagem'];
		}
		
		$this->db->where('id', $this->input->post('id'));

		if ($this->db->update('produtos', $dd)) {
			return true;
		} else {
			return false;	
		}

	}
	
	function remover($id) {
		
		$this->db->where('id', $id);
		$this->db->delete('produtos');	
		
	}

	// VALIDA A NAVEGAÇÃO

########### FUNÇÃO PARA DATAS #######################
	// converter para data dd/mm/aaaa
	function converte_data($data){
		
		if (strstr($data, "/")) {//verifica se tem a barra /
		
		  $d = explode ("/", $data);//tira a barra
		  $invert_data = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
		  return $invert_data;
		
		} elseif(strstr($data, "-")) {
		
		  $d = explode ("-", $data);
		  $invert_data = "$d[2]/$d[1]/$d[0]";
		  return $invert_data;
		
		} else {
		
		  return "Data invalida";
		
		}
	
	}
	
}
?>