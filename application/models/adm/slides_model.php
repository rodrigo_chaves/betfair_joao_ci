<?php
class Slides_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}

	function getSlides($limit=99){

		$qr = $this->db->query("SELECT * FROM slides LIMIT $limit");		
		return $qr;	
	}
	
	function getSlide($id){
		
		$qr = $this->db->query("SELECT * FROM slides WHERE id = ".$id);		
		return $qr;	
		
	}	
	
	function cadastrar(){
		$dd = array('titulo' => $_POST['titulo'],
					'link' => $_POST['link'],
					'chamada' => $_POST['chamada'],
					'status' => $_POST['status'],
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img'] = $_POST['imagem'];
		}
		
		if ($this->db->insert('slides', $dd)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	function editar(){
		$dd = array('titulo' => $_POST['titulo'],
					'link' => $_POST['link'],
					'chamada' => $_POST['chamada'],
					'status' => $_POST['status'],
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img'] = $_POST['imagem'];
		}
		
		$this->db->where('id', $_POST['id']);
		if ($this->db->update('slides', $dd)) {
			return true;
		} else {
			return false;
		}
	}
	
	function remover($id) {
		
		$this->db->where('id', $id);
		$this->db->delete('slides');	
		
	}

	// VALIDA A NAVEGAÇÃO

########### FUNÇÃO PARA DATAS #######################
	// converter para data dd/mm/aaaa
	function converte_data($data){
		
		if (strstr($data, "/")) {//verifica se tem a barra /
		
		  $d = explode ("/", $data);//tira a barra
		  $invert_data = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
		  return $invert_data;
		
		} elseif(strstr($data, "-")) {
		
		  $d = explode ("-", $data);
		  $invert_data = "$d[2]/$d[1]/$d[0]";
		  return $invert_data;
		
		} else {
		
		  return "Data invalida";
		
		}
	
	}
	
}
?>