<?php
class Servicos_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}
	
	function cadastrar(){
		//print_r($this->input->post('cores'));
		
		$dd = array('nome' => $this->input->post('nome'),
					'id_categoria' => $this->input->post('categoria'),
					'descricao' => $this->input->post('descricao')
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img_portfolio'] = $_POST['imagem'];
		}
		
		if ($this->db->insert('servicos', $dd)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	function editar(){
		
		$dd = array('nome' => $this->input->post('nome'),
					'id_categoria' => $this->input->post('categoria'),
					'descricao' => $this->input->post('descricao')
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img_portfolio'] = $_POST['imagem'];
		}
		
		$this->db->where('id', $this->input->post('id'));

		if ($this->db->update('servicos', $dd)) {
			return true;
		} else {
			return false;	
		}

	}
	
	function remover($id) {
		
		$this->db->where('id', $id);
		$this->db->delete('servicos');	
		
	}

	// VALIDA A NAVEGAÇÃO

########### FUNÇÃO PARA DATAS #######################
	// converter para data dd/mm/aaaa
	function converte_data($data){
		
		if (strstr($data, "/")) {//verifica se tem a barra /
		
		  $d = explode ("/", $data);//tira a barra
		  $invert_data = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
		  return $invert_data;
		
		} elseif(strstr($data, "-")) {
		
		  $d = explode ("-", $data);
		  $invert_data = "$d[2]/$d[1]/$d[0]";
		  return $invert_data;
		
		} else {
		
		  return "Data invalida";
		
		}
	
	}
	
}
?>