<?php
class Unidades_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}

	
	

	
	function cadastrar($tipo='cad') {
		
		$dd = array(
					'titulo' => $_POST['titulo'],
					'email' => $_POST['email'],
					'telefones' => $_POST['telefones'],
					'cep' => $_POST['cep'],
					'endereco' => $_POST['endereco'],
					'numero' => $_POST['numero'],
					'bairro' => $_POST['bairro'],
					'cidade' => $_POST['cidade'],
					'uf' => $_POST['uf'],
					'descricao' => $_POST['descricao'],
					
					);
		
		if ($tipo == 'cad') {
			$this->db->insert('unidades', $dd);
		} 
		if($tipo == 'edit'){
			$this->db->where('id',$_POST['id']);
			$this->db->update('unidades', $dd);
		}	 
	
	}

}
?>