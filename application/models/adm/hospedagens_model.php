<?php
class Hospedagens_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}

	
	
	function set_hospedagem() {
		
		#echo "OK";
		$dd = array(					
					'id_user' => $this->session->userdata('id'),
					'nome' => $this->input->post('nome'),
					'local' => $this->input->post('local'),
					'lon' => $this->input->post('lon'),
					'lat' => $this->input->post('lat'),
					'tipo' => $this->input->post('tipo'),					
					'cep' => $this->input->post('cep'),
					'endereco' => $this->input->post('endereco'),
					'numero' => $this->input->post('numero'),
					'complemento' => $this->input->post('complemento'),
					'cidade' => $this->input->post('cidade'),
					'bairro' => $this->input->post('bairro'),
					'uf' => $this->input->post('uf'),
					'n_quartos' => $this->input->post('n_quartos'),
					'descricao' => $this->input->post('descricao')
					
		);
				
		$this->db->insert('hospedagens', $dd);
		#$id_hosp = $this->db->insert_id();
		#return $id_hosp;
			
		#redirect('home/cadastro/seted/'.$id_user);
	
	}
	
	function set_quartos(){
		#print_r($_POST);
		
		#echo $this->input->post('cama_casal'.$q);
		#return false;
		
		#echo "<br />";
		$id_hospedagem = $this->input->post('id_hospedagem');
		$n_quartos = $this->input->post('n_quartos');
		
		//echo $preco.'<br>';
		for($q=0; $q<$n_quartos; $q++){
			$tipo_quarto = $this->input->post('tipo'.$q);
			$preco = str_replace(",",".",$this->input->post('preco'.$q));
			
			// defini o tipo
			$solteiro = $this->input->post('cama_solteiro'.$q);
			$casal = $this->input->post('cama_casal'.$q) * 2;
			$beliche =  $this->input->post('beliche'.$q) * 2;
			$rede  = $this->input->post('redes'.$q);
			$acomoda_tipo = $solteiro+$casal+$beliche+$rede;
			
			if($acomoda_tipo == 1){
				$tipo = 'Simples';
			}
			if($acomoda_tipo == 2){
				$tipo = 'Duplo';
			}
			if($acomoda_tipo == 3){
				$tipo = 'Triplo';
			}
			if($acomoda_tipo == 4){
				$tipo = 'Quádruplo';
			}
			if($acomoda_tipo == 5){
				$tipo = 'Quintuplo';
			}
			if($acomoda_tipo == 6){
				$tipo = 'Sêxtuplo';
			}
			if($acomoda_tipo == 7){
				$tipo = 'Sétuplo';
			}
			if($acomoda_tipo == 8){
				$tipo = 'Óctuplo';
			}
			if($acomoda_tipo == 9){
				$tipo = 'Nônuplo';
			}
			if($acomoda_tipo == 10){
				$tipo = 'Décuplo';
			}
			if($acomoda_tipo > 10){
				$tipo = 'Mais';
			}
			
			#$acomoda += $acomoda_q;
			
			$dd_quarto = array(
				'id_hospedagem' => $this->input->post('id_hospedagem'),
				'nome' => $this->input->post('nome'.$q),
				'tipo' => $tipo,
				'cama_solteiro' => $this->input->post('cama_solteiro'.$q),
				'cama_casal' => $this->input->post('cama_casal'.$q),
				'beliche' => $this->input->post('beliche'.$q),
				'rede' => $this->input->post('redes'.$q),
				'preco' => $preco
			);
			
			if(isset($_POST['compartilhado'.$q])){
				$dd_quarto['compartilhado'] = 1;
			}
			
			// day use
			if(isset($_POST['day_use'.$q])){
				$dd_quarto['day_use'] = 1;
				$preco = str_replace(",",".",$this->input->post('day_use_preco'.$q));
				$dd_quarto['day_use_preco'] = $preco;				
				if(isset($_POST['almoco'.$q])){
					$dd_quarto['day_use_almoco'] = 1;
				}
				
				
			}
			
			
			// horarioguel
			if(isset($_POST['horarioguel'.$q])){
				$dd_quarto['horarioguel'] = 1;
				
				$preco_2h = str_replace(",",".",$this->input->post('ate_2h'.$q));
				$preco_4h = str_replace(",",".",$this->input->post('ate_4h'.$q));
				
				$dd_quarto['ate_2h'] = $preco_2h;				
				$dd_quarto['ate_4h'] = $preco_4h;				
				
				
			}
			
			
			// oferta
			
			if(isset($_POST['oferta'.$q])){
				$dd_quarto['oferta'] = 1;
				
				$dd_quarto['oferta_de'] = $this->padrao_model->converte_data($_POST['oferta_de'.$q]);
				$dd_quarto['oferta_para'] = $this->padrao_model->converte_data($_POST['oferta_para'.$q]);
				$dd_quarto['oferta_preco'] = str_replace(",",".",$this->input->post('oferta_preco'.$q));
				$dd_quarto['oferta_descricao'] = $this->input->post('oferta_descricao'.$q);
				
			}
			$this->db->insert('hospedagens_quartos' , $dd_quarto);
			
			
		}
		
		
	}
	
	function set_info_adicionais(){
		
		
		$id_hospedagem = $this->input->post('id_hospedagem');
		
		// convenio
		if(isset($_POST['convenio'])){
			$dd_conv = array('convenio' => 1);
		}else{
			$dd_conv = array('convenio' => 0);
		}
		
		if(isset($_POST['nome'])){
			$dd_conv['nome'] = $this->input->post('nome');
			$dd_conv['tipo'] = $this->input->post('tipo');
		}
		
		$this->db->where('id',$id_hospedagem);
		$this->db->update('hospedagens' , $dd_conv);
		
		
		
		// alimentacao		
		$qr_alimentacao = $this->db->get('hospedagens_alimentacao');
		foreach($qr_alimentacao->result() as $a){
			
			$dd_up = array(
				'id_hospedagem' => $id_hospedagem,
				'id_alimentacao' => $a->id
			);
			if(isset($_POST['ali'.$a->id])){				
				$this->db->where($dd_up);
				$qr_verifica_a = $this->db->get('hospedagens_alimentacao_on');				
				if($qr_verifica_a->num_rows() == 0){
					$this->db->insert('hospedagens_alimentacao_on' , $dd_up);
				}
			}else{
			
				$this->db->where($dd_up);
				$qr_verifica = $this->db->get('hospedagens_alimentacao_on');
				if($qr_verifica->num_rows() > 0){
					$this->db->where($dd_up);
					$this->db->delete('hospedagens_alimentacao_on');
				}
			}
		}
		
		// facilidades		
		$qr_facilidades = $this->db->get('hospedagens_facilidades');
		foreach($qr_facilidades->result() as $f){
			$dd_up = array(
				'id_hospedagem' => $id_hospedagem,
				'id_facilidade' => $f->id
			);
			if(isset($_POST['fac'.$f->id])){			
				$this->db->where($dd_up);
				$qr_verifica_f = $this->db->get('hospedagens_facilidade_on');				
				if($qr_verifica_f->num_rows() == 0){
					$this->db->insert('hospedagens_facilidade_on' , $dd_up);
				}
			}else{
				$this->db->where($dd_up);
				$qr_verifica = $this->db->get('hospedagens_facilidade_on');
				if($qr_verifica->num_rows() > 0){
					$this->db->where($dd_up);
					$this->db->delete('hospedagens_facilidade_on');
				}
			}
		}
		
		
		
	}
	
	
	function set_foto(){
		
			################ ANEXO
			if ($anexos = $this->input->post('anexos')) {		
				foreach ($anexos as $anexo) :
					$dd = array('id_hospedagem' => $this->input->post('id_hospedagem'),
								'foto' => $anexo);
					$this->db->insert('hospedagens_fotos', $dd);					
				endforeach;		
				#redirect('dash/hospedagens/seted');
				redirect('dash/set_info_adicionais/'.$this->input->post('id_hospedagem').'/seted');
			}else{
				redirect('dash/hospedagens/doc_not');
				
			}
			
	}
	
	function del_foto_hospedagem($id_anexo){
		$dd_anexo = $this->padrao_model->get_by_id($id_anexo,'hospedagens_fotos')->row();
		#if($dd_anexo->id_user == $this->session->userdata('id')){
			unlink("files/".$dd_anexo->foto);
			$this->db->where('id',$id_anexo);
			$this->db->delete('hospedagens_fotos');
			redirect('dash/set_info_adicionais/'.$dd_anexo->id_hospedagem.'/img_rem');
		#}else{
		#	redirect("dash/verificacao");
		#}
	}
	
	// mostra hospedagens da rota
	function get_hosp_por_rota($id_rota){
		
		$dd_rota = $this->padrao_model->get_by_id($id_rota,'rotas')->row();
		$paradas = $this->padrao_model->get_by_matriz('id_rota',$id_rota,'rotas_paradas');
		
		#$de = $dd_rota->de;
		$para = $dd_rota->para;
		
		// pega apenas uma parte do local (cidade)
		#echo $de;
		$cidade_para = explode(",", $para);
		#echo $cidade_para[0]; // piece1
		
		
		$where_paradas = "";
		if($paradas->num_rows() > 0){
			foreach($paradas->result() as $parada){
				
				$cidade_parada = explode(",", $parada->cidade);
				$where_paradas .= "OR cidade LIKE '%".$cidade_parada[0]."%' "; 
				#echo $cidade_parada[0].'<br>';
			}
		}
		
		$qr = $this->db->query("SELECT * FROM hospedagens WHERE (cidade LIKE '%".$cidade_para[0]."%' ) $where_paradas  "); 
		
		#echo $de." <strong>Para</strong> ".$para;
		#return false;
		
		return $qr;
		
		
	}
	
	function set_hospedagem_na_rota(){
		$id_rota = $this->input->post('id_rota_hosp');
		#$hosps = $this->db->input('hosps');
		
		
		$hosps = $_POST['hosps'];
		foreach($hosps as $key){
			$dd = array(
				'id_rota' => $id_rota,
				'id_hospedagem' => $key
			);
			
			$this->db->where($dd);
			$qr_verifica = $this->db->get('rotas_hospedagens');
			if($qr_verifica->num_rows() == 0){
				$this->db->insert('rotas_hospedagens' , $dd);
			}
		}
		redirect('dash/rotas/hosp_set');
	}
	
	// front-end
	function get_hosp_home(){
		#INNER JOIN hospedagens_fotos hf ON hf.id_hospedagem = h.id 
		$qr = $this->db->query("SELECT * FROM hospedagens order by id desc limit 10");
		return $qr;
	}
	
	function get_hosp_cidade($cidade=''){
		if($cidade == ''){
			$cidade = $_POST['cidade'];
		}
		
		$qr = $this->db->query("SELECT * FROM hospedagens WHERE cidade LIKE '%$cidade%' order by id desc limit 10");
		return $qr;
		
	}
	
	function get_acomoda($id_hosp,$tipo='toda'){
		// todos os quartos
		if($tipo == 'toda'){
			$quartos = $this->padrao_model->get_by_matriz('id_hospedagem',$id_hosp,'hospedagens_quartos');
			$acomoda = 0;
			foreach($quartos->result() as $quarto){
				$solteiro = $quarto->cama_solteiro;
				$casal = $quarto->cama_casal * 2;
				$beliche =  $quarto->beliche * 2;
				$rede  = $quarto->rede;
				$acomoda_q = $solteiro+$casal+$beliche+$rede;
				$acomoda += $acomoda_q;
			}
			
			
			
			
		}
		
		// apenas um quarto
		if($tipo == 'quarto'){
			$quarto = $this->padrao_model->get_by_id($id_hosp,'hospedagens_quartos')->row();
			$solteiro = $quarto->cama_solteiro;
			$casal = $quarto->cama_casal * 2;
			$beliche =  $quarto->beliche * 2;
			$rede  = $quarto->rede;
			$acomoda_q = $solteiro+$casal+$beliche+$rede;
			$acomoda += $acomoda_q;
		}
		
		echo $acomoda;		
		
		
		
	}
	
	// verifica opções dos quartos
	function get_opc_quarto_por_hospedagem($id_hospedagem,$campo='compartilhado'){
		$where = array(
			'id_hospedagem' => $id_hospedagem,
			$campo => 1
		);
		$this->db->where($where);
		$qr = $this->db->get('hospedagens_quartos');
		return $qr;
	}
	
	// verifica opções dos quartos
	function get_opc_quarto($id_quarto,$campo='compartilhado'){
		$where = array(
			'id' => $id_quarto,
			$campo => 1
		);
		$this->db->where($where);
		$qr = $this->db->get('hospedagens_quartos');
		return $qr;
	}
	
	
	function excluir_hosp($id_hospedagem){
		
		$dd_hosp = $this->padrao_model->get_by_id($id_hospedagem,'hospedagens')->row();
		#echo $id_hospedagem;
		$reservas = $this->padrao_model->get_by_matriz('id_hospedagem',$id_hospedagem,'hospedagens_reservas');
		if($reservas->num_rows() > 0){
			redirect('dash/hospedagens/hosp_reser');
		}
		if($dd_hosp->id_user == $this->session->userdata('id')){
			$this->db->where('id',$id_hospedagem);
			$this->db->delete('hospedagens');
			redirect('dash/hospedagens/hosp_del');
		}
		
	}
	
	######### RESERVAR
	function reservar($id_quarto){
		#print_r($_POST);
		#return false;
		if(!$this->session->userdata('id')){
			redirect('home/login');
		}
		
		$quarto = $this->padrao_model->get_by_id($id_quarto,'hospedagens_quartos')->row();		
		$hosp = $this->padrao_model->get_by_id($quarto->id_hospedagem,'hospedagens')->row();		
		$dd_user = $this->padrao_model->get_by_id($hosp->id_user,'usuarios')->row();
		
		
		
		
		
		$dd_reserva = array(
			'id_hospedagem' => $hosp->id,
			'id_quarto' => $quarto->id,
			'id_user' => $this->session->userdata('id'),
			'checkin' => $this->padrao_model->converte_data($this->input->post('checkin')),
			'checkout' => $this->padrao_model->converte_data($this->input->post('checkout')),
			'hospedes' => $this->input->post('hospedes')
			
		);
		
		$this->db->insert('hospedagens_reservas' , $dd_reserva);
		$id_reserva = $this->db->insert_id();
		

		#echo $id_reserva;
		#return false;
		### NOTIFICACOES
		$dd_not = array(
			'id_user' => $hosp->id_user,
			'id_hospedagem' => $quarto->id,
			'titulo' => "Pedido de Reserva!" ,
			'info' => "Você recebeu uma reserva na sua hospedagem cadastrada no 1Todo. - Durmobem. Local <strong>".$hodp->nome." </strong> - <strong>".$hosp->cidade."</strong>"
		);
		$this->db->insert('notificacoes' , $dd_not);
		
		$telefone = $this->padrao_model->get_by_id($hosp->id_user,'usuarios')->row()->telefone1;
		##### ENVIA O SMS
		$user="felipe.dantas@yahoo.com.br"; 
		$password="felipe951"; 
		$codpessoa="3355";
		#$to="3499999999;1199988888"; //Até 500 números por envio
		$to=$telefone; //Até 500 números por envio
		#$msg="Seu PIN de confirmacao Matriz card: $pin. Sua senha de transacao: $senha2"; 
		$msg="Você recebeu uma reserva na sua hospedagem cadastrada no www.1Todo.com.br - Durmobem. Local: ".$hosp->nome;
		$enviarimediato="S"; //S ou N
		$data=""; //exemplo 20/03/2012
		$hora=""; //exemplo 20:15
		$msg = URLEncode($msg);
		$response= file_get_contents("http://web.smscel.com.br/sms/views/getsmscaction.do?user=".$user."&password=".$password."&codpessoa=".$codpessoa."&to=".$to."&msg=".$msg."&enviarimediato=".$enviarimediato."&data=".$data."&hora=".$hora);
		
		
		redirect('durmobem/reservar/'.$id_quarto.'/seted');
		
	}
	
	
	function get_reservas_anfitriao($id_user='',$tipo='hospedagens'){
		/*
		$qr = $this->db->query("SELECT hr.id_quarto FROM hospedagens_reservas hr
								INNER JOIN hospedagens_quartos hq ON hq.id_hospedagem = hr.id_quarto 
								 
								");
		*/
		#echo "OK";
		#return false;
		if($id_user == ''){
			$id_user = $this->session->userdata('id');
		}
		
		if($tipo == 'hospedagens'){
			$qr_hospedagens = $this->padrao_model->get_by_matriz('id_user',$id_user,'hospedagens'); 
			$where = "id_hospedagem = ".$qr_hospedagens->row()->id." ";
			#echo $qr_hospedagens->num_rows();
			foreach($qr_hospedagens->result() as $hospedagem){
				$where .= " OR id_hospedagem = ".$hospedagem->id." ";
			}
			#echo $where;
			$qr_reservas = $this->db->query("SELECT * FROM hospedagens_reservas WHERE $where");
			return $qr_reservas;
		}
		
		if($tipo == 'eventos'){
			$qr_eventos = $this->padrao_model->get_by_matriz('id_user',$id_user,'eventos'); 
			if($qr_eventos->num_rows() == 0){
				redirect('dash/eventos');
			}
			$where = "id_evento = ".$qr_eventos->row()->id." ";
			#echo $qr_hospedagens->num_rows();
			foreach($qr_eventos->result() as $evento){
				$where .= " OR id_evento = ".$evento->id." ";
			}
			#echo $where;
			$qr_reservas = $this->db->query("SELECT * FROM eventos_reservas WHERE $where ORDER BY id desc");
			return $qr_reservas;
		}
	}
	
	function set_res_hosp($id_reserva,$status){
		$dd_reserva = $this->padrao_model->get_by_id($id_reserva,'hospedagens_reservas')->row();
		$dd_user = $this->padrao_model->get_by_id($dd_reserva->id_user,'usuarios')->row();
		$telefone = $dd_user->telefone1;
		
		
		if($status == 1){
			$titulo =  "Reserva confirmada";
			$descricao = "<a href='".base_url()."durmobem/hospedagem/".$dd_reserva->id_hospedagem."'>Seu pedido de reserva foi aceito. Entre no site ".base_url()." e veja os detalhes.</a>";
			$descricao_sms = "Seu pedido de reserva foi aceito. Entre no site ".base_url()."  e veja os detalhes.";
		}
		
		if($status == 2){
			$titulo =  "Reserva cancelada";
			$descricao = "<a href='".base_url()."durmobem/hospedagem/".$dd_reserva->id_hospedagem."'>Seu pedido de reserva foi cancelado. Entre no site ".base_url()." e veja os detalhes.</a>";
			$descricao_sms = "Seu pedido de reserva foi cancelado. Entre no site ".base_url()."  e veja os detalhes.";
		}
		
		##### ENVIA O SMS
		$user="felipe.dantas@yahoo.com.br"; 
		$password="felipe951"; 
		$codpessoa="3355";
		#$to="3499999999;1199988888"; //Até 500 números por envio
		$to=$telefone; //Até 500 números por envio
		#$msg="Seu PIN de confirmacao Matriz card: $pin. Sua senha de transacao: $senha2"; 
		$msg=$descricao_sms;
		$enviarimediato="S"; //S ou N
		$data=""; //exemplo 20/03/2012
		$hora=""; //exemplo 20:15
		$msg = URLEncode($msg);
		$response= file_get_contents("http://web.smscel.com.br/sms/views/getsmscaction.do?user=".$user."&password=".$password."&codpessoa=".$codpessoa."&to=".$to."&msg=".$msg."&enviarimediato=".$enviarimediato."&data=".$data."&hora=".$hora);
		
		
		### NOTIFICACOES
		$dd_not = array(
			'id_user' => $dd_user->id,
			'id_hospedagem' => $dd_reserva->id_hospedagem,
			'titulo' => $titulo,
			'info' => $descricao
		);
		$this->db->insert('notificacoes' , $dd_not);
		/////////////////////// 
		
		
		// informa os dados do cancelamento caso seja;
		if($status == 2){
			$dd = array(
				'status' => $status,
				'cancelamento_motivo' => $this->input->post('motivo'),
				'cancelamento_descricao' => $this->input->post('descricao'),
			);
			$this->db->where('id',$id_reserva);
			$this->db->update('hospedagens_reservas' , $dd);
			redirect('dash/reservas_hospedagens/rem_reserva');
		}else{
			$dd = array('status' => $status);
			$this->db->where('id',$id_reserva);
			$this->db->update('hospedagens_reservas' , $dd);
			redirect('dash/reservas_hospedagens');
		}
	}
	
	
	function cancelar_reserva($id_reserva){
		$dd_reserva = $this->padrao_model->get_by_id($id_reserva,'hospedagens_reservas')->row();
		$dd_hosp = $this->padrao_model->get_by_id($dd_reserva->id_hospedagem,'hospedagens')->row();
		if($dd_reserva->id_user == $this->session->userdata('id')){
			#$new_dd = array('status' => 2);			
			$new_dd = array(
				'status' => 2,
				'cancelamento_motivo' => $this->input->post('motivo'),
				'cancelamento_descricao' => $this->input->post('descricao'),
			);
			
			$this->db->where('id',$id_reserva);
			$this->db->update('hospedagens_reservas' , $new_dd);
			
			
			### NOTIFICACOES
			$titulo =  "Hospedagem cancelada";
			$descricao = "<a href='".base_url()."dash/reservas_hospedagens/'>Um usuário Cancelou a carona. Entre no site ".base_url()."  e veja os detalhes da viagem.</a>";
			$dd_not = array(
				'id_user' => $dd_rota->id_user,
				'id_hospedagem' => $dd_hosp->id,
				'titulo' => $titulo,
				'info' => $descricao
			);
			$this->db->insert('notificacoes' , $dd_not);
			
			$telefone = $this->padrao_model->get_by_id($dd_hosp->id_user,'usuarios')->row()->telefone1;
			##### ENVIA O SMS
			$user="felipe.dantas@yahoo.com.br"; 
			$password="felipe951"; 
			$codpessoa="3355"; 
			#$to="3499999999;1199988888"; //Até 500 números por envio
			$to=$telefone; //Até 500 números por envio
			#$msg="Seu PIN de confirmacao Matriz card: $pin. Sua senha de transacao: $senha2"; 
			$msg=strip_tags($descricao);;
			$enviarimediato="S"; //S ou N
			$data=""; //exemplo 20/03/2012
			$hora=""; //exemplo 20:15
			$msg = URLEncode($msg);
			$response= file_get_contents("http://web.smscel.com.br/sms/views/getsmscaction.do?user=".$user."&password=".$password."&codpessoa=".$codpessoa."&to=".$to."&msg=".$msg."&enviarimediato=".$enviarimediato."&data=".$data."&hora=".$hora);
			
			
			redirect('dash/caronas/rem_hosp');
		}else{
			redirect('dash');
		}
	}
	
	
	
	function busca(){
		
		//$campos = 2;
		
		$checkin = $this->padrao_model->converte_data($this->input->post('checkin'));
		$checkout = $this->padrao_model->converte_data($this->input->post('checkout'));
		$tipo = $this->input->post('tipo');
		$para = $this->input->post('para');
		
		$valor_de = $this->input->post('valor_de');
		$valor_ate = $this->input->post('valor_ate');

		
		
		
		$where = "cidade LIKE '%$para%'";
		
		#echo $dt_partida.'<br />';
		#return false;
		
		
		if($tipo <> ""){
			//$campos++;
			$where .= " AND (tipo = '$tipo')";
		}
		
		if(isset($_POST['day_use'])){
			$campos++;
			$where .= "AND (hq.day_use <> '1')";
		} 
		
		if(isset($_POST['horarioguel'])){
			$campos++;
			$where .= "AND (hq.horarioguel = 1)";
		} 
		if(isset($_POST['oferta'])){
			$campos++;
			$where .= "AND (hq.oferta = 1)";
		} 
		/*
		if($hora_partida_de <> "" && $hora_partida_ate <> ""){
			$campos++;
			$where .= " AND (hora_saida BETWEEN '$hora_partida_de' AND '$hora_partida_ate')";
		}
		*/
		if($valor_de <> "" && $valor_ate <> ""){
			$campos++;
			$where .= " AND (hq.preco BETWEEN '$valor_de' AND '$valor_ate')";
		}
		
		#echo $campos;
		
		#$qr = $this->db->query("SELECT * FROM hospedagens WHERE $where");	
		
		$qr = $this->db->query("SELECT h.id, h.nome, h.id_user, h.tipo , h.status, h.cidade, h.uf , hq.day_use
						FROM hospedagens h
						INNER JOIN hospedagens_quartos hq ON h.id = hq.id_hospedagem
						WHERE $where");	
			
		
		#$qr = $this->db->query("SELECT * FROM rotas WHERE $where ORDER BY dt_saida asc");
		
		#echo $de.' '.$para;
		#echo $qr->num_rows().' - '.$campos;
		#echo "<br />";
		#echo $where;
		#return false;
		
		return $qr;
	}
	
} // x class
?>