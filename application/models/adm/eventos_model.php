<?php
class Eventos_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}

	
	
	function set_evento() {
		#$preco = str_replace(",",".",$this->input->post('preco'));
		#echo "OK";
		$dt_hora_termino = $this->padrao_model->converte_data($this->input->post('dt_termino')).' '.$this->input->post('hora_termino');
		$dd = array(					
					'id_user' => $this->session->userdata('id'),
					#'nome' => $this->input->post('nome'),
					#'local' => $this->input->post('local'),
					#'lon' => $this->input->post('lon'),
					#'lat' => $this->input->post('lat'),
					#'tipo' => $this->input->post('tipo'),					
					'cep' => $this->input->post('cep'),
					'endereco' => $this->input->post('endereco'),
					'numero' => $this->input->post('numero'),
					'complemento' => $this->input->post('complemento'),
					'cidade' => $this->input->post('cidade'),
					'bairro' => $this->input->post('bairro'),
					'uf' => $this->input->post('uf'),
					
					'dt_inicio' => $this->padrao_model->converte_data($this->input->post('dt_inicio')),
					'hora_inicio' => $this->input->post('hora_inicio'),
					'dt_termino' => $this->padrao_model->converte_data($this->input->post('dt_termino')),
					'hora_termino' => $this->input->post('hora_termino'),
					
					'dt_hora_termino' => $dt_hora_termino,
					
					#'preco' => $preco,					
					'minimo' => $this->input->post('minimo'),
					'maximo' => $this->input->post('maximo'),
					'descricao' => $this->input->post('descricao')
					
		);
		
		if($_POST['dt_inicio_incricao'] <> '' || $_POST['dt_termino_inscricao'] <> ''){			
			$dd['dt_inicio_inscricao'] = $this->padrao_model->converte_data($this->input->post('dt_inicio_inscricao'));
			$dd['hora_inicio_inscricao'] = $this->input->post('hora_inicio_inscricao');
			$dd['dt_termino_inscricao'] = $this->padrao_model->converte_data($this->input->post('dt_termino_inscricao'));
			$dd['hora_termino_inscricao'] = $this->input->post('hora_termino_inscricao');
		}
				
		$this->db->insert('eventos', $dd);
		#echo "OK";
		$id_hosp = $this->db->insert_id();
		return $id_hosp;
			
		#redirect('home/cadastro/seted/'.$id_user);
	
	}
	
	function set_evento_list(){
		
		$id_evento = $this->input->post('id_evento');
		$nome = $this->input->post('nome');
		$preco = str_replace(",",".",$this->input->post('preco'));
		
		$dd_evento = array(
			'nome' => $nome,
			'preco' => $preco,
			'status' => 1
		);
		
		$this->db->where('id',$id_evento);
		$this->db->update('eventos' , $dd_evento);
		
		//echo $preco.'<br>';
		for($q=1; $q<11; $q++){
			if($_POST['item'.$q] <> '' or $_POST['participante'.$q] <> ''){
				$participante = $this->input->post('participante'.$q);
				$item = $this->input->post('item'.$q);
				$qtd = $this->input->post('qtd'.$q);
				$preco = str_replace(",",".",$this->input->post('preco'.$q));
				
				
				
				$dd_quarto = array(
					'id_evento' => $this->input->post('id_evento'),
					'participante' => $participante,
					'item' => $item,
					'qtd' => $qtd,
					'preco' => $preco
					
				);
				
				$this->db->insert('eventos_list' , $dd_quarto);
			}
			
		}
		$qr_fotos = $this->padrao_model->get_by_matriz('id_evento',$id_evento,'eventos_fotos');
		if($qr_fotos->num_rows() == 0){
			redirect('dash/fotos_evento/'.$id_evento);	
		}
		
		redirect('dash/set_evento/'.$id_evento);
		
		
	}
	
	function excluir_item_evento($id_item){
		$dd_item = $this->padrao_model->get_by_id($id_item,'eventos_list')->row();
		$dd_evento = $this->padrao_model->get_by_id($dd_item->id_evento,'eventos')->row();
		$id_user = $dd_evento->id_user;
		if($id_user == $this->session->userdata('id')){
			$this->db->where('id',$id_item);
			$this->db->delete('eventos_list');
			redirect('dash/set_evento/'.$dd_evento->id);
		}else{
			redirect('dash/');
		}
	}
	
	
	
	
	
	function set_foto(){
		
			################ ANEXO
			if ($anexos = $this->input->post('anexos')) {		
				foreach ($anexos as $anexo) :
					$dd = array('id_evento' => $this->input->post('id_evento'),
								'foto' => $anexo);
					$this->db->insert('eventos_fotos', $dd);					
				endforeach;		
				redirect('dash/fotos_evento/'.$this->input->post('id_evento'));
			}else{
				redirect('dash/eventos/doc_not');
				
			}
			
	}
	
	function del_foto_evento($id_foto){
		$dd_foto = $this->padrao_model->get_by_id($id_foto,'eventos_fotos')->row();
		$dd_evento = $this->padrao_model->get_by_id($dd_foto->id_evento,'eventos')->row();
		$id_user = $dd_evento->id_user;
		#echo $id_user.' '.$this->session->userdata('id');
		#return false;
		if($id_user == $this->session->userdata('id')){
			$this->db->where('id',$id_foto);
			$this->db->delete('eventos_fotos');
			redirect('dash/fotos_evento/'.$dd_evento->id);
		}else{
			redirect('dash/');
		}
	}
	
	function set_info_adicionais(){
		$id_evento = $this->input->post('id_evento');
		
		// facilidades		
		$qr_facilidades = $this->db->get('hospedagens_facilidades');
		foreach($qr_facilidades->result() as $f){
			$dd_up = array(
				'id_evento' => $id_evento,
				'id_facilidade' => $f->id
			);
			if(isset($_POST['fac'.$f->id])){			
				$this->db->where($dd_up);
				$qr_verifica_f = $this->db->get('eventos_facilidades');				
				if($qr_verifica_f->num_rows() == 0){
					$this->db->insert('eventos_facilidades' , $dd_up);
				}
			}else{
				$this->db->where($dd_up);
				$qr_verifica = $this->db->get('eventos_facilidades');
				if($qr_verifica->num_rows() > 0){
					$this->db->where($dd_up);
					$this->db->delete('eventos_facilidades');
				}
			}
		}
		
		
		// ajudantes		
		$qr_ajudantes = $this->db->get('ajudantes');
		foreach($qr_ajudantes->result() as $a){
			$dd_up = array(
				'id_evento' => $id_evento,
				'id_ajudante' => $a->id
			);
			if(isset($_POST['aju'.$a->id])){			
				$this->db->where($dd_up);
				$qr_verifica_f = $this->db->get('eventos_ajudantes');				
				if($qr_verifica_f->num_rows() == 0){
					$this->db->insert('eventos_ajudantes' , $dd_up);
				}
			}else{
				$this->db->where($dd_up);
				$qr_verifica = $this->db->get('eventos_ajudantes');
				if($qr_verifica->num_rows() > 0){
					$this->db->where($dd_up);
					$this->db->delete('eventos_ajudantes');
				}
			}
		}
		redirect('dash/evento_info_adicionais/'.$id_evento);
	}
	
	function del_foto_hospedagem($id_anexo){
		$dd_anexo = $this->padrao_model->get_by_id($id_anexo,'hospedagens_fotos')->row();
		#if($dd_anexo->id_user == $this->session->userdata('id')){
			unlink("files/".$dd_anexo->foto);
			$this->db->where('id',$id_anexo);
			$this->db->delete('hospedagens_fotos');
			redirect('dash/hospedagens/img_seted');
		#}else{
		#	redirect("dash/verificacao");
		#}
	}
	
	// mostra eventos da rota
	function get_evento_por_rota($id_rota){
		
		$dd_rota = $this->padrao_model->get_by_id($id_rota,'rotas')->row();
		$paradas = $this->padrao_model->get_by_matriz('id_rota',$id_rota,'rotas_paradas');
		
		#$de = $dd_rota->de;
		$para = $dd_rota->para;
		
		// pega apenas uma parte do local (cidade)
		#echo $de;
		$cidade_para = explode(",", $para);
		#echo $cidade_para[0]; // piece1
		
		
		$where_paradas = "";
		if($paradas->num_rows() > 0){
			foreach($paradas->result() as $parada){
				
				$cidade_parada = explode(",", $parada->cidade);
				$where_paradas .= "OR cidade LIKE '%".$cidade_parada[0]."%' "; 
				#echo $cidade_parada[0].'<br>';
			}
		}
		
		$qr = $this->db->query("SELECT * FROM eventos WHERE (cidade LIKE '%".$cidade_para[0]."%' ) $where_paradas  "); 
		
		#echo $de." <strong>Para</strong> ".$para;
		#return false;
		
		return $qr;
		
		
	}
	
	function set_hospedagem_na_rota(){
		$id_rota = $this->input->post('id_rota_hosp');
		#$hosps = $this->db->input('hosps');
		
		
		$hosps = $_POST['hosps'];
		foreach($hosps as $key){
			$dd = array(
				'id_rota' => $id_rota,
				'id_hospedagem' => $key
			);
			
			$this->db->where($dd);
			$qr_verifica = $this->db->get('rotas_hospedagens');
			if($qr_verifica->num_rows() == 0){
				$this->db->insert('rotas_hospedagens' , $dd);
			}
		}
		redirect('dash/rotas/hosp_set');
	}
	
	
	function excluir_evento($id_evento){
		$dd_evento = $this->padrao_model->get_by_id($id_evento,'eventos')->row();
		$id_user = $dd_evento->id_user;
		#echo $id_user.' '.$this->session->userdata('id');
		#return false;
		if($id_user == $this->session->userdata('id')){
			$dd_stat = array('status' => 2);
			$this->db->where('id',$id_evento);
			$this->db->update('eventos' , $dd_stat);
			
			#### ENVIA NOTIFICAÇÃO PARA QUEM ESTÁ INSCRITO
			
			
			redirect('dash/eventos/rem');
		}else{
			redirect('dash/');
		}
	}
	
	// front-end
	function get_eventos_home(){
		#INNER JOIN hospedagens_fotos hf ON hf.id_hospedagem = h.id 
		$qr = $this->db->query("SELECT * FROM eventos WHERE status = '1' AND dt_termino > NOW() order by id desc limit 10");
		return $qr;
	}
	
	function get_eventos_cidade($cidade=''){
		
		$where = " (status = '1' AND dt_termino > NOW()) ";
		
		if(isset($_POST['nome_evento'])){
			$nome = $_POST['nome_evento'];
			$where .= " AND nome LIKE '%$nome%' ";
		}
		
		if(isset($_POST['local_evento'])){
			$local = $_POST['local_evento'];
			$where .= " AND cidade LIKE '%$cidade%' ";
		}
		
		if($_POST['data_evento'] <> ''){
			$data = $this->padrao_model->converte_data($_POST['data_evento']);
			$where .= " AND dt_inicio BETWEEN  '$data' AND '$data'";
		}
		
		$qr = $this->db->query("SELECT * FROM eventos WHERE  $where  order by dt_inicio desc");
		
		if($_POST['participante_evento'] <> ''){
			$anfitriao = $_POST['participante_evento'];
			$this->db->query("SELECT * FROM usuarios WHERE nome LIKE '%$anfitriao%'");
			$where .= " AND nome LIKE '%$nome%' ";
			$qr = $this->db->query("SELECT e.id, e.preco, e.dt_inicio, e.hora_inicio FROM eventos e 
									INNER JOIN usuarios u ON u.id = e.id_user
									WHERE  (e.status = '1' AND e.dt_termino > NOW()) AND (u.nome LIKE '%$anfitriao%')  order by dt_inicio desc
									 									
									");
		}
		
		
		return $qr;
		
	}
	

	
	
	######### RESERVAR
	function reservar($id_evento){
		#print_r($_POST);
		#return false;
		if(!$this->session->userdata('id')){
			redirect('home/login');
		}
		
		$evento = $this->padrao_model->get_by_id($id_evento,'eventos')->row();		
		$dd_user = $this->padrao_model->get_by_id($evento->id_user,'usuarios')->row(); // anfitriao
		
		
		
		
		
		$dd_reserva = array(
			'id_evento' => $id_evento,
			'id_user' => $this->session->userdata('id'),
			'representa' => $this->input->post('representa')			
		);
		
		$this->db->insert('eventos_reservas' , $dd_reserva);
		$id_reserva = $this->db->insert_id();
		

		#echo $id_reserva;
		#return false;
		### NOTIFICACOES
		$dd_not = array(
			'id_user' => $dd_user->id,
			'id_evento' => $id_reserva,
			'titulo' => "Pedido de Reserva!" ,
			'info' => "Você recebeu uma reserva na seu evento cadastrado no 1Todo - Curtabem. Local <strong>".$evento->nome." </strong> - <strong>".$evento->cidade."</strong>"
		);
		$this->db->insert('notificacoes' , $dd_not);
		
		$telefone = $this->padrao_model->get_by_id($dd_user->id,'usuarios')->row()->telefone1;
		##### ENVIA O SMS
		$user="felipe.dantas@yahoo.com.br"; 
		$password="felipe951"; 
		$codpessoa="3355";
		#$to="3499999999;1199988888"; //Até 500 números por envio
		$to=$telefone; //Até 500 números por envio
		#$msg="Seu PIN de confirmacao Matriz card: $pin. Sua senha de transacao: $senha2"; 
		$msg="Você recebeu uma reserva no seu evento cadastrada no www.1todo.com.br - Curtabem. Local: ".$evento->nome;
		$enviarimediato="S"; //S ou N
		$data=""; //exemplo 20/03/2012
		$hora=""; //exemplo 20:15
		$msg = URLEncode($msg);
		$response= file_get_contents("http://web.smscel.com.br/sms/views/getsmscaction.do?user=".$user."&password=".$password."&codpessoa=".$codpessoa."&to=".$to."&msg=".$msg."&enviarimediato=".$enviarimediato."&data=".$data."&hora=".$hora);
		
		
		redirect('curtabem/evento/'.$id_evento.'/seted');
		
	}
	
	
	function get_reservas_anfitriao($id_user=''){
		/*
		$qr = $this->db->query("SELECT hr.id_quarto FROM hospedagens_reservas hr
								INNER JOIN hospedagens_quartos hq ON hq.id_hospedagem = hr.id_quarto 
								 
								");
		*/
		#echo "OK";
		#return false;
		if($id_user == ''){
			$id_user = $this->session->userdata('id');
		}
		$qr_hospedagens = $this->padrao_model->get_by_matriz('id_user',$id_user,'hospedagens'); 
		$where = "id_hospedagem = ".$qr_hospedagens->row()->id." ";
		#echo $qr_hospedagens->num_rows();
		foreach($qr_hospedagens->result() as $hospedagem){
			$where .= " OR id_hospedagem = ".$hospedagem->id." ";
		}
		#echo $where;
		$qr_reservas = $this->db->query("SELECT * FROM hospedagens_reservas WHERE $where");
		return $qr_reservas;
	}
	
	function set_res_eve($id_reserva,$status){
		$dd_reserva = $this->padrao_model->get_by_id($id_reserva,'eventos_reservas')->row();
		$dd_user = $this->padrao_model->get_by_id($dd_reserva->id_user,'usuarios')->row();
		$telefone = $dd_user->telefone1;
		
		
		if($status == 1){
			$titulo =  "Reserva confirmada";
			$descricao = "<a href='".base_url()."curtabem/evento/".$dd_reserva->id_evento."'>Seu pedido de reserva foi aceito. Entre no site ".base_url()." e veja os detalhes.</a>";
			$descricao_sms = "Seu pedido de reserva foi aceito. Entre no site ".base_url()."  e veja os detalhes.";
		}
		
		if($status == 2){
			$titulo =  "Reserva cancelada";
			$descricao = "<a href='".base_url()."curtabem/evento/".$dd_reserva->id_evemto."'>Seu pedido de reserva foi cancelado. Entre no site ".base_url()." e veja os detalhes.</a>";
			$descricao_sms = "Seu pedido de reserva foi cancelado. Entre no site ".base_url()."  e veja os detalhes.";
		}
		
		##### ENVIA O SMS
		$user="felipe.dantas@yahoo.com.br"; 
		$password="felipe951"; 
		$codpessoa="3355";
		#$to="3499999999;1199988888"; //Até 500 números por envio
		$to=$telefone; //Até 500 números por envio
		#$msg="Seu PIN de confirmacao Matriz card: $pin. Sua senha de transacao: $senha2"; 
		$msg=$descricao_sms;
		$enviarimediato="S"; //S ou N
		$data=""; //exemplo 20/03/2012
		$hora=""; //exemplo 20:15
		#$msg = URLEncode($msg);
		$response= file_get_contents("http://web.smscel.com.br/sms/views/getsmscaction.do?user=".$user."&password=".$password."&codpessoa=".$codpessoa."&to=".$to."&msg=".$msg."&enviarimediato=".$enviarimediato."&data=".$data."&hora=".$hora);
		
		
		### NOTIFICACOES
		$dd_not = array(
			'id_user' => $dd_user->id,
			'id_evento' => $dd_reserva->id_evento,
			'titulo' => $titulo,
			'info' => $descricao
		);
		$this->db->insert('notificacoes' , $dd_not);
		/////////////////////// 
		
		if($status == 2){
			$dd = array(
				'status' => $status,
				'cancelamento_motivo' => $this->input->post('motivo'),
				'cancelamento_descricao' => $this->input->post('descricao'),
			);
			$this->db->where('id',$id_reserva);
			$this->db->update('eventos_reservas' , $dd);			
			redirect('dash/reservas_eventos/can_res');
		}else{
			$dd = array('status' => $status);
			$this->db->where('id',$id_reserva);
			$this->db->update('eventos_reservas' , $dd);
			redirect('dash/reservas_eventos/seted');
		}
	}
	
	function cancelar_reserva($id_reserva){
		$dd_reserva = $this->padrao_model->get_by_id($id_reserva,'eventos_reservas')->row();
		$dd_eve = $this->padrao_model->get_by_id($dd_reserva->id_evento,'eventos')->row();
		if($dd_reserva->id_user == $this->session->userdata('id')){
			#$new_dd = array('status' => 2);
			$new_dd = array(
				'status' => 2,
				'cancelamento_motivo' => $this->input->post('motivo'),
				'cancelamento_descricao' => $this->input->post('descricao'),
			);
			$this->db->where('id',$id_reserva);
			$this->db->update('eventos_reservas' , $new_dd);
			
			
			### NOTIFICACOES
			$titulo =  "Reserva do Evento cancelada";
			$descricao = "<a href='".base_url()."dash/caronas/'>Um usuário Cancelou a ída no evento. Entre no site ".base_url()."  e veja os detalhes.</a>";
			$dd_not = array(
				'id_user' => $dd_eve->id_user,
				'id_evento' => $dd_eve->id,
				'titulo' => $titulo,
				'info' => $descricao
			);
			$this->db->insert('notificacoes' , $dd_not);
			
			$telefone = $this->padrao_model->get_by_id($dd_eve->id_user,'usuarios')->row()->telefone1;
			##### ENVIA O SMS
			$user="felipe.dantas@yahoo.com.br"; 
			$password="felipe951"; 
			$codpessoa="3355"; 
			#$to="3499999999;1199988888"; //Até 500 números por envio
			$to=$telefone; //Até 500 números por envio
			#$msg="Seu PIN de confirmacao Matriz card: $pin. Sua senha de transacao: $senha2"; 
			$msg=strip_tags($descricao);;
			$enviarimediato="S"; //S ou N
			$data=""; //exemplo 20/03/2012
			$hora=""; //exemplo 20:15
			$msg = URLEncode($msg);
			$response= file_get_contents("http://web.smscel.com.br/sms/views/getsmscaction.do?user=".$user."&password=".$password."&codpessoa=".$codpessoa."&to=".$to."&msg=".$msg."&enviarimediato=".$enviarimediato."&data=".$data."&hora=".$hora);
			
			
			redirect('dash/caronas');
		}else{
			redirect('dash');
		}
	}
	
	
} // x class
?>