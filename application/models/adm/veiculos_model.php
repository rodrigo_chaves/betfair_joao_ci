<?php
class Veiculos_model extends CI_Model{	
	
	function _construct()
	{
		// Call the Model constructor
		parent::_construct();
	}
	
	function getVeiculos(){

		$qr = $this->db->query("SELECT v.id, v.categoria, c.nome, v.tipo, v.modelo, v.chamada, v.descricao, v.img_portfolio FROM veiculos v
									INNER JOIN categorias c ON c.id = v.categoria ORDER BY id desc");		
		return $qr;	
	}

	function getVeiculo($id){

		$veiculo = $this->db->query("SELECT v.id, v.modelo, v.descricao, v.chamada, v.descricao, v.preco, c.nome as categoria FROM veiculos v
												INNER JOIN categorias c ON c.id = v.categoria
												WHERE v.id = ?", $id)->row();		
												
		return $veiculo;	
	}
		
	function getCategorias(){

		$qr = $this->db->query("SELECT n.categoria, (	SELECT COUNT( * ) 
													FROM noticias
													WHERE categoria = n.categoria
													) as qtd
													FROM noticias n
													GROUP BY n.categoria");
		
		return $qr;	
	}
	
	function getNoticiasCategoria($categoria) {		
		
		$qr = $this->db->query("SELECT * FROM noticias WHERE categoria = '$categoria'");		
		return $qr;			
	
	}
	
	function cadastrar(){
		//print_r($this->input->post('cores'));
		
		$dd = array('tipo' => $this->input->post('tipo'),
					'modelo' => $this->input->post('modelo'),
					'tipo' => $this->input->post('tipo'),//moto, triciclo, quadriciclo, automovel etc
					'categoria' => $this->input->post('categoria'),
					'chamada' => $this->input->post('chamada'),
					'descricao' => $this->input->post('descricao')
					);
		
		if (isset($_POST['imagem'])) {
			$dd['img_portfolio'] = $_POST['imagem'];
		}
		
		$cores  = $this->input->post('cores');
		
		
		
		if ($this->db->insert('veiculos', $dd)) {
			
			$id_veiculo = $this->db->insert_id();

			foreach($cores as $cor) :
				$ddd = array('id_veiculo' => $id_veiculo,
								'cor' => $cor);
				$this->db->insert('veiculos_cores', $ddd);
			endforeach;
			
			return true;
		} else {
			return false;
		}
		
	}
	
	
	function editar(){
		
		$dd = array('tipo' => $this->input->post('tipo'),
			'modelo' => $this->input->post('modelo'),
			'tipo' => $this->input->post('tipo'),//moto, triciclo, quadriciclo, automovel etc
			'categoria' => $this->input->post('categoria'),
			'chamada' => $this->input->post('chamada'),
			'descricao' => $this->input->post('descricao')
			);
		
		if (isset($_POST['imagem'])) {
			$dd['img_portfolio'] = $_POST['imagem'];
		}
		
		$cores  = $this->input->post('cores');
		

		$this->db->where('id', $this->input->post('id'));
		if ($this->db->update('veiculos', $dd)) {
			
			$this->db->where('id_veiculo', $this->input->post('id'));			
			$this->db->delete('veiculos_cores');

			foreach($cores as $cor) :
				
				$ddd = array('id_veiculo' => $this->input->post('id'),
								'cor' => $cor);
				$this->db->insert('veiculos_cores', $ddd);
			endforeach;
			
			return true;
		} else {
			
			return false;	
		}
				

	}
	
	function remover($id) {
		
		$this->db->where('id', $id);
		$this->db->delete('veiculos');	
		$this->db->where('id_veiculo', $id);
		$this->db->delete('veiculos_cores');	
		$this->db->where('id_veiculo', $id);
		$this->db->delete('veiculos_fotos');	
		
	}

	// VALIDA A NAVEGAÇÃO

########### FUNÇÃO PARA DATAS #######################
	// converter para data dd/mm/aaaa
	function converte_data($data){
		
		if (strstr($data, "/")) {//verifica se tem a barra /
		
		  $d = explode ("/", $data);//tira a barra
		  $invert_data = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
		  return $invert_data;
		
		} elseif(strstr($data, "-")) {
		
		  $d = explode ("-", $data);
		  $invert_data = "$d[2]/$d[1]/$d[0]";
		  return $invert_data;
		
		} else {
		
		  return "Data invalida";
		
		}
	
	}
	
}
?>