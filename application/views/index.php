<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title> MMN - Site. </title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" type="<?php echo base_url()?>" href="favicon.ico">	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/motioncss.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/motioncss-widgets.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/style.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/responsive.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/animation.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/colors/color_nl.css">	
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/rs-settings.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>css/2014/magnific-popup.css">
    <meta name="google-site-verification" content="8T1hw9nj5T12kXxRUm0Vd4BCMGkiH5WUuWFEqCTCqp8" />
</head>

<body class="l-body">

	<!-- CANVAS -->
	<div class="l-canvas type_wide col_cont headerpos_fixed headertype_extended">
		<div class="l-canvas-h">

			<?php include('includes/2014/header.php');?>

			<!-- MAIN -->
			<div class="l-main">
				<div class="l-main-h">

						<!-- Slider -->
						<div class="l-submain type_fullwidth border_none">
							<div class="l-submain-h g-html">
								<div class="g-cols">
									<div class="full-width">
										<div class="fullwidthbanner-container">
											<div class="fullwidthbanner">
												<ul>

													<!-- SLIDE ODONTO 
		                                            <li data-transition="boxslide" data-slotamount="7">
		                                              
		                                              <img src="<?php echo base_url();?>img/servicos/chair.jpg" data-bgrepeat="no-repeat" data-bgfit="cover" data-bgposition="center center">

		                                              <div class="tp-caption medium_text amarelo sft bg_white"  
		                                              data-x="0" 
		                                              data-y="94" 
		                                              data-speed="700" 
		                                              data-start="1700" 
		                                              data-easing="easeOutBack"><strong>ZERO CUSTO DE ADESÃO</strong> (Unidades limitadas)!</div>

		                                              <div class="tp-caption large_text sft bg_white"  
		                                              data-x="0" 
		                                              data-y="140" 
		                                              data-speed="700" 
		                                              data-start="1750" 
		                                              data-easing="easeOutBack">
		                                              Adiquira um Site Otimizado para<br>
		                                              seu Consultório Odontológico</div>
		                                            
		                                              
		                                              <div class="tp-caption medium_text amarelo sfb bg_white"  
		                                              data-x="0" 
		                                              data-y="240" 
		                                              data-speed="500" 
		                                              data-start="1800" 
		                                              data-easing="easeOutBack">
		                                              O <strong>melhor custo benefício</strong> do mercado para obter um site<br> de alta qualidade focado para consultórios e dentistas.</div>
		                                              
		                                              <div class="tp-caption sfb"
		                                              data-x="604"
		                                              data-y="50"
		                                              data-speed="400"
		                                              data-start="2200"
		                                              data-easing="easeInOutSine"><img src="<?php echo base_url()?>img/servicos/mobile.png" /></div>

		                                              <div class="tp-caption sfb bg_white"
		                                              data-x="0"
		                                              data-y="312"
		                                              data-speed="400"
		                                              data-start="2000"
		                                              data-easing="easeInOutSine">
		                                              <a class="g-btn type_primary size_big" href="<?php echo base_url()?>servicos/odontoPlanos">
		                                               <span><i class="fa fa-check-square-o"></i> Ver planos e Preços</span>
		                                              </a>
		                                              </div>
		                                            </li> -->

		                                            <!-- SLIDE MOBILE SITE  -->
		                                            <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" data-thumb="<?php echo base_url()?>img/servicos/slide3-320x200.jpg"  data-saveperformance="off"  data-title="Slide">
		                                                
		                                                <!-- MAIN IMAGE -->
		                                                <img src="<?php echo base_url()?>img/servicos/slide3.jpg"  alt="slide3"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
		                                                
		                                                <!-- LAYER NR. 1 -->
		                                                <div class="tp-caption astra_bg_dark_heading_1 sfr" 
		                                                    data-x="0" 
		                                                    data-y="160"  
		                                                    data-speed="500" 
		                                                    data-start="800" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-splitin="none" 
		                                                    data-splitout="none" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                    data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Mobile Site 
		                                                </div>

		                                                <!-- LAYER NR. 2 -->
		                                                <div class="tp-caption astra_dark_big_text sfr" 
		                                                    data-x="0" 
		                                                    data-y="260"  
		                                                    data-speed="500" 
		                                                    data-start="1000" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-splitin="none" 
		                                                    data-splitout="none" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                    data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">72% dos usuários de smartphones<br> acessam a web todos os dias em seus aparelhos.
		                                                </div>

		                                                <div class="tp-caption sfb"
															data-x="0" 
		                                                    data-y="340"  
		                                                    data-speed="500" 
		                                                    data-start="1100" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-splitin="none" 
		                                                    data-splitout="none" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                    data-end="8700" 
		                                                    data-endspeed="300"> 
															<a class="g-btn type_primary size_big" href="<?php echo base_url()?>servicos/CriacaoDeSites">
															<span><i class="fa fa-check-square-o"></i> Saiba Mais</span>
															</a>
														</div>

		                                            </li>
		                                            
		                                            <!-- SLIDE MULTIPLOS DEVICES  -->
		                                            <li data-transition="parallaxtotop" data-slotamount="7" data-masterspeed="1000" data-thumb="<?php echo base_url()?>img/servicos/slide4-320x200.jpg"  data-saveperformance="off"  data-title="Slide">
		                                                
		                                                <img src="<?php echo base_url()?>img/servicos/slide4.jpg"  alt="slide4"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">                                                

		                                                <!-- LAYER NR. 1 -->
		                                                <div class="tp-caption lfl" 
		                                                    data-x="510" 
		                                                    data-y="82"  
		                                                    data-speed="600" 
		                                                    data-start="600" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                    data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 5;"><img src="<?php echo base_url()?>img/servicos/desktop.png" alt=""> 
		                                                </div>

		                                                <!-- LAYER NR. 2 -->
		                                                <div class="tp-caption lfr" 
		                                                     data-x="390" 
		                                                     data-y="213"  
		                                                    data-speed="600" 
		                                                    data-start="800" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                     data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 6;"><img src="<?php echo base_url()?>img/servicos/pad_portrait.png" alt=""> 
		                                                </div>

		                                                <!-- LAYER NR. 3 -->
		                                                <div class="tp-caption lfr" 
		                                                     data-x="780" 
		                                                     data-y="256"  
		                                                    data-speed="600" 
		                                                    data-start="1000" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                     data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 7;"><img src="<?php echo base_url()?>img/servicos/pad_landscape.png" alt=""> 
		                                                </div>

		                                                <!-- LAYER NR. 4 -->
		                                                <div class="tp-caption lft" 
		                                                    data-x="568" 
		                                                    data-y="296"  
		                                                    data-speed="600" 
		                                                    data-start="1200" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                    data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 8;"><img src="<?php echo base_url()?>img/servicos/phone.png" alt=""> 
		                                                </div>

		                                                <!-- LAYER NR. 5 -->
		                                                <div class="tp-caption astra_white_heading_1 sft" 
		                                                    data-x="0" 
		                                                    data-y="140"  
		                                                    data-speed="600" 
		                                                    data-start="1600" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-splitin="none" 
		                                                    data-splitout="none" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                    data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Seu site sempre ao<br> alcance do seu cliente.
		                                                </div>

		                                                <!-- LAYER NR. 6 -->
		                                                <div class="tp-caption astra_white_big_text sfb" 
		                                                    data-x="0" 
		                                                    data-y="240"  
		                                                    data-speed="600" 
		                                                    data-start="1800" 
		                                                    data-easing="easeOutExpo" 
		                                                    data-splitin="none" 
		                                                    data-splitout="none" 
		                                                    data-elementdelay="0" 
		                                                    data-endelementdelay="0" 
		                                                    data-end="8700" 
		                                                    data-endspeed="300" 
		                                                    style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">A forma de consumir mudou <br> e esse consumidor multicanal exige<br> adaptações das marcas aos<br> seus hábitos e costumes.
		                                                </div>
		                                            </li>

													
		                                        </ul>
											</div>
										</div>
								
									</div>
								</div>
							</div>
						</div>

						<!-- Features -->
						<?php include('includes/2014/features.php');?>										



						<!-- Serviços -->

						<div class="l-submain">
							<div class="l-submain-h g-html">
								<h1 style="text-align: center;">Nossos Serviços</h1>								
								<div class="g-hr type_short">
									<span class="g-hr-h">
										<i class="fa fa-star"></i>
									</span>
								</div>

								<?php include ("includes/2014/servicos.php");?>							

								<p style="text-align: center; padding:60px 0 0;">
									<a class="g-btn type_outline" href="<?php echo base_url()?>servicos">
									<span>Ver mais serviços</span></a>
								</p>
							</div>
						</div>	

						<!-- Serviços -->



						<!-- Trabalhos recentes -->

						<div class="l-submain">

							<div class="l-submain-h g-html">

								<h1 style="text-align: center;">Trabalhos Recentes</h1>								

								<div class="g-hr type_short">
									<span class="g-hr-h">
										<i class="fa fa-star"></i>
									</span>
								</div>

								<div class="w-portfolio columns_3">

									<div class="w-portfolio-h">
										<div class="w-portfolio-list">
											<div class="w-portfolio-list-h">
												<? foreach($portfolios->result() as $port){ ?>
                                                <div class="w-portfolio-item order_1 naming webdesign">
                                                    <div class="w-portfolio-item-h">
                                                        <a class="w-portfolio-item-anchor" href="<?php echo base_url()?>novo/portfolio_cliente/<?=url_title($port->titulo)?>/<?=$port->id?>">
                                                            <div class="w-portfolio-item-image">
                                                                <img src="<?php echo base_url().$port->imagem?>" alt="<?=$port->titulo?>"/>
                                                                <div class="w-portfolio-item-meta">
                                                                    <h2 class="w-portfolio-item-title"><?=$port->titulo?></h2>
                                                                    <i class="fa fa-mail-forward"></i>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <? } // x foreach ?>        
        
                                            </div>
										</div>
									</div>
								</div>

								<p style="text-align: center; padding:60px 0 0;"><a class="g-btn type_outline" href="<?php echo base_url()?>novo/portfolio"><span>Ver mais trabalhos</span></a></p>

							</div>
						</div>

						
						<?php include ("includes/2014/clientes.php");?>

						<?php include("includes/2014/newsletter.php");?>

					</div>
				</div>
			</div>
			<!-- /MAIN -->
		</div>
	</div>
	<!-- /CANVAS -->



<!-- FOOTER -->
<?php include('includes/2014/footer2.php');?>

<!-- INÍCIO 

     JAVASCRIPT'S:
     ============================================== -->

     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery-1.9.1.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/g-alert.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.carousello.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.flexslider.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.isotope.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.magnific-popup.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.parallax.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.simpleplaceholder.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.smoothScroll.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.themepunch.plugins.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/jquery.themepunch.revolution.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/plugins.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/waypoints.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/w-lang.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/w-search.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/w-tabs.js"></script>
     <script type="text/javascript" src="<?php echo base_url()?>js/2014/w-timeline.js"></script>

     <script>jQuery(window).load(function(){ jQuery('#parallax_section').parallax('50%', '-1.2'); });</script> 
     
     <!--Start of Zopim Live Chat Script-->
     <script type="text/javascript">
     window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
     d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
     _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
     $.src="//v2.zopim.com/?1bPzfTvgW1Zyf8QHkgcjJc5AizcX03UV";z.t=+new Date;$.
     type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
     </script>
     <!--End of Zopim Live Chat Script-->

</body>
</html>

