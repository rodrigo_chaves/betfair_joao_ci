﻿<tr ng-repeat-start="(marketId, event) in vm.tableData.events" ng-if="event.isReady &amp;&amp; event.isVisible">
    <td>
        <a class="mod-link" ng-transclude="" data-link-type="MARKET" data-event-type-id="1" data-market-id="1.138698158" ng-click="::vm.events.sendAnalytics(event)" href="https://www.betfair.com/exchange/plus/football/market/1.138698158">
            <bf-event-line event-data="::event">
                <!---->
                <section class="mod-event-line" data-ng-if="vm.eventData" data-bf-below-width="{&#39;small&#39;: 300, &#39;medium&#39;: 350, &#39;large&#39;: 400}">
                    <data-bf-livescores event-data="vm.eventData" mini="">
                        <section class="bf-livescores mini">
                            <!----><!---->
                            <div data-ng-if="livescoresCtrl.ready" data-ng-include="::livescoresCtrl.templateUrl">
                                <div class="default-template">
                                    <!---->
                                    <data-bf-livescores-start-date data-ng-if="!livescoresCtrl.templates.hasScores">
                                        <!---->
                                        <ng-include src="::startDateCtrl.templateUrl">
                                            <div class="bf-livescores-start-date" data-ng-class="{&#39;in-play&#39;: startDateCtrl.isInPlay}">
                                                <div class="start-date-wrapper"><span class="label">Hoje 18:00</span></div>
                                            </div>
                                        </ng-include>
                                    </data-bf-livescores-start-date>
                                    <!----><!----><!---->
                                </div>
                            </div>
                            <!---->
                        </section>
                    </data-bf-livescores>
                    <ul class="runners">
                        <li title="PSG">PSG **</li>
                        <li title="Dijon">Di
                        jon</li>
                    </ul>
                    <ul class="bet-counters">
                        <li class="bet-counter unmatched bf-tooltip-parent" data-ng-class="{visible: vm.eventData.unmatchedBetsCount &gt; 0}">
                            <span class="bet-counter-indicator">0</span>
                            <data-bf-tooltip options="top center">
                                <div class="bf-tooltip-wrapper top center">
                                    <div class="bf-tooltip" ng-class="{&#39;has-close-button&#39;:closeButton==&#39;true&#39; , &#39;visible&#39;:visible==true}">
                                        <div class="text" ng-transclude=""><span class="bet-counter-tooltip-count"></span> <span>Apostas não correspondidas</span></div>
                                        <!---->
                                    </div>
                                </div>
                            </data-bf-tooltip>
                        </li>
                        <li class="bet-counter matched bf-tooltip-parent" data-ng-class="{visible: vm.eventData.matchedBetsCount &gt; 0}">
                            <span class="bet-counter-indicator">0</span>
                            <data-bf-tooltip options="top center">
                                <div class="bf-tooltip-wrapper top center">
                                    <div class="bf-tooltip" ng-class="{&#39;has-close-button&#39;:closeButton==&#39;true&#39; , &#39;visible&#39;:visible==true}">
                                        <div class="text" ng-transclude=""><span class="bet-counter-tooltip-count"></span> <span>Apostas correspondidas</span></div>
                                        <!---->
                                    </div>
                                </div>
                            </data-bf-tooltip>
                        </li>
                    </ul>
                    <!---->
                    <ul class="matched-amount" title="Correspondido: $205,279" data-ng-if="vm.eventData.matchedAmount !== undefined">
                        <li class="matched-amount-value">$205,279</li>
                    </ul>
                    <!---->
                    <ul class="icons">
                        <!---->
                        <li class="icon-tv" title="SportTV1" data-ng-if="::vm.eventData.broadcastTvChannels">
                            <svg>
                                <use xlink:href="/resources/eds/bundle/images/sprite-bf-event-line_3504_.svg#ontv"></use>
                            </svg>
                        </li>
                        <!----><!---->
                    </ul>
                </section>
                <!---->
            </bf-event-line>
        </a>
    </td>
    <td class="coupon-runners" colspan="3">
        <!-- ODDS HOME-->
        <div class="coupon-runner" ng-repeat="runner in event.runners">
            <button class="bf-bet-button back-button back-selection-button" data-bet-type="back" data-ng-class="::{&#39;is-sp&#39;: isSp}" type="back" depth="0" price="1.09" size="$10049" to-fraction="vm.events.getFractionalOdd(runner.back.fractionalOdd)" highlight="false" ng-click="vm.events.betButtonClick(runner.key, runner.selectionId, vm.data.betTypeConfiguration.back, runner.back.price, runner.handicap, marketId, event.marketName, runner.name)" ng-disabled="vm.data.isDisabled(event, runner.selectionId)" is-selected="false" selection-id="65323">
                <!----><!---->
                <div class="bf-bet-button-info" data-ng-if="::!isSp"><span class="bet-button-price">1.09</span> <span class="bet-button-size">$10049</span></div>
                <!---->
            </button>
            <!---->
            <button class="bf-bet-button lay-button lay-selection-button" data-bet-type="lay" data-ng-class="::{&#39;is-sp&#39;: isSp}" ng-if="vm.showLays" type="lay" depth="0" price="1.1" size="$30336" to-fraction="vm.events.getFractionalOdd(runner.lay.fractionalOdd)" highlight="false" ng-click="vm.events.betButtonClick(runner.key, runner.selectionId, vm.data.betTypeConfiguration.lay, runner.lay.price, runner.handicap, marketId, event.marketName, runner.name)" ng-disabled="vm.data.isDisabled(event, runner.selectionId)" is-selected="false" selection-id="65323">
                <!----><!---->
                <div class="bf-bet-button-info" data-ng-if="::!isSp"><span class="bet-button-price">1.1</span> <span class="bet-button-size">$30336</span></div>
                <!---->
            </button>
            <!---->
        </div>
        <!-- ODDS DRAW -->
        <div class="coupon-runner" ng-repeat="runner in event.runners">
            <button class="bf-bet-button back-button back-selection-button" data-bet-type="back" data-ng-class="::{&#39;is-sp&#39;: isSp}" type="back" depth="0" price="15.5" size="$104" to-fraction="vm.events.getFractionalOdd(runner.back.fractionalOdd)" highlight="false" ng-click="vm.events.betButtonClick(runner.key, runner.selectionId, vm.data.betTypeConfiguration.back, runner.back.price, runner.handicap, marketId, event.marketName, runner.name)" ng-disabled="vm.data.isDisabled(event, runner.selectionId)" is-selected="false" selection-id="58805">
                <!----><!---->
                <div class="bf-bet-button-info" data-ng-if="::!isSp"><span class="bet-button-price">15.5</span> <span class="bet-button-size">$104</span></div>
                <!---->
            </button>
            <!---->
            <button class="bf-bet-button lay-button lay-selection-button" data-bet-type="lay" data-ng-class="::{&#39;is-sp&#39;: isSp}" ng-if="vm.showLays" type="lay" depth="0" price="16.5" size="$46" to-fraction="vm.events.getFractionalOdd(runner.lay.fractionalOdd)" highlight="false" ng-click="vm.events.betButtonClick(runner.key, runner.selectionId, vm.data.betTypeConfiguration.lay, runner.lay.price, runner.handicap, marketId, event.marketName, runner.name)" ng-disabled="vm.data.isDisabled(event, runner.selectionId)" is-selected="false" selection-id="58805">
                <!----><!---->
                <div class="bf-bet-button-info" data-ng-if="::!isSp"><span class="bet-button-price">16.5</span> <span class="bet-button-size">$46</span></div>
                <!---->
            </button>
            <!---->
        </div>
        <!-- ODDS AWAY -->
        <div class="coupon-runner" ng-repeat="runner in event.runners">
            <button class="bf-bet-button back-button back-selection-button" data-bet-type="back" data-ng-class="::{&#39;is-sp&#39;: isSp}" type="back" depth="0" price="44" size="$62" to-fraction="vm.events.getFractionalOdd(runner.back.fractionalOdd)" highlight="false" ng-click="vm.events.betButtonClick(runner.key, runner.selectionId, vm.data.betTypeConfiguration.back, runner.back.price, runner.handicap, marketId, event.marketName, runner.name)" ng-disabled="vm.data.isDisabled(event, runner.selectionId)" is-selected="false" selection-id="305995">
                <!----><!---->
                <div class="bf-bet-button-info" data-ng-if="::!isSp"><span class="bet-button-price">44</span> <span class="bet-button-size">$62</span></div>
                <!---->
            </button>
            <!---->
            <button class="bf-bet-button lay-button lay-selection-button" data-bet-type="lay" data-ng-class="::{&#39;is-sp&#39;: isSp}" ng-if="vm.showLays" type="lay" depth="0" price="50" size="$96" to-fraction="vm.events.getFractionalOdd(runner.lay.fractionalOdd)" highlight="false" ng-click="vm.events.betButtonClick(runner.key, runner.selectionId, vm.data.betTypeConfiguration.lay, runner.lay.price, runner.handicap, marketId, event.marketName, runner.name)" ng-disabled="vm.data.isDisabled(event, runner.selectionId)" is-selected="false" selection-id="305995">
                <!----><!---->
                <div class="bf-bet-button-info" data-ng-if="::!isSp"><span class="bet-button-price">50</span> <span class="bet-button-size">$96</span></div>
                <!---->
            </button>
            <!---->
        </div>
        <!----><!---->
    </td>
	
    <td class="coupon-market-rules"><a class="market-rules-icon" ng-click="::vm.events.showMarketRules(marketId)" ng-attr-title="{{::&#39;MARKETVIEW.RULES.LITERAL&#39; | i18n}}" title="Regras"></a></td>

</tr>